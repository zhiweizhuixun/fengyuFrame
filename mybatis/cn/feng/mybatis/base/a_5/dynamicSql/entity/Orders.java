package cn.feng.mybatis.base.a_5.dynamicSql.entity;

import java.util.Date;
import java.util.List;

import cn.feng.mybatis.base.a_2.test.entity.User;

public class Orders {
	private int id;
	private int userId;
	private String number;
	private Date createtime;
	private String node;
	//用户信息
	private User user;
	
	//订单明细
	private List<Orderdetail> orderdetails;
	
	
	public List<Orderdetail> getOrderdetails() {
		return orderdetails;
	}

	public void setOrderdetails(List<Orderdetail> orderdetails) {
		this.orderdetails = orderdetails;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Date getCreateTimeDate() {
		return createtime;
	}

	public void setCreateTimeDate(Date createTimeDate) {
		this.createtime = createTimeDate;
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}

	@Override
	public String toString() {
		return "Orders [id=" + id + ", userId=" + userId + ", number=" + number
				+ ", createTimeDate=" + createtime + ", node=" + node
				+ ", user=" + user + "]";
	}
	
}
