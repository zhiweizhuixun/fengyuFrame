package cn.feng.mybatis.base.a_5.dynamicSql.mapper;

import java.util.List;

import cn.feng.mybatis.base.a_2.test.entity.User;
import cn.feng.mybatis.base.a_5.dynamicSql.entity.Orders;
import cn.feng.mybatis.base.a_5.dynamicSql.entity.OrdersCustom;

public interface OrdersMapper {
	/*******实现一对一查询小结实现一对一查询：*******/
	// 2.4resultType和resultMap实现一对一查询小结实现一对一查询：
	// resultType：使用resultType实现较为简单，如果pojo中没有包括查询出来的列名，需要增加列名对应的属性，即可完成映射。
	// 如果没有查询结果的特殊要求建议使用resultType。
	//
	// resultMap：需要单独定义resultMap，实现有点麻烦，如果对查询结果有特殊的要求，
	// 使用resultMap可以完成将关联查询映射pojo的属性中 。
	//
	// resultMap可以实现延迟加载，resultType无法实现延迟加载。
	//
	public List<OrdersCustom> getOrdersAndUser() throws Exception;

	// 查询顶大关联查询用户使用resultMap
	public List<Orders> getOrdersAndUserResultMap() throws Exception;
	
	/*************************实现3一对多查询：****************/
	
	// mybatis使用resultMap的collection对关联查询的多条记录映射到一个list集合属性中。
	//
	// 使用resultType实现：
	// 将订单明细映射到orders中的orderdetails中，需要自己处理，使用双重循环遍历，去掉重复记录，将订单明细放在orderdetails中。

	// 查询订单(关联用户)及订单明细
	public List<Orders> findOrderAndOrderDetailResultMap() throws Exception;
	
	
	/*************************多对多查询：****************/
	public List<User> findUserAndItemsResutlMap() throws Exception;
	/*将查询用户购买的商品信息明细清单，（用户名、用户地址、购买商品名称、购买商品时间、购买商品数量）

	针对上边的需求就使用resultType将查询到的记录映射到一个扩展的pojo中，很简单实现明细清单的功能。

	一对多是多对多的特例，如下需求：
	查询用户购买的商品信息，用户和商品的关系是多对多关系。
	需求1：
	查询字段：用户账号、用户名称、用户性别、商品名称、商品价格(最常见)
	企业开发中常见明细列表，用户购买商品明细列表，
	使用resultType将上边查询列映射到pojo输出。

	需求2：
	查询字段：用户账号、用户名称、购买商品数量、商品明细（鼠标移上显示明细）
	使用resultMap将用户购买的商品明细列表映射到user对象中。

	总结：

	使用resultMap是针对那些对查询结果映射有特殊要求的功能，，比如特殊要求映射成list中包括 多个list。*/
	/*************************实现3一对多查询：****************/
	
	
	/*************************实现3一对多查询：****************/
	/*************************实现3一对多查询：****************/
}
