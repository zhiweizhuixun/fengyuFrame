package cn.feng.mybatis.base.a_5.dynamicSql.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import cn.feng.mybatis.base.a_2.test.entity.User;
import cn.feng.mybatis.base.a_5.dynamicSql.entity.Orders;
import cn.feng.mybatis.base.a_5.dynamicSql.entity.OrdersCustom;
import cn.feng.mybatis.base.a_5.dynamicSql.mapper.OrdersMapper;

public class OrdersMapperTest {
	private static SqlSessionFactory sqlSessionFactory = null;

	/**
	 * 加载配置
	 */
	@Before
	public void config() throws IOException {
		// mybatis配置文件
		String resource = "mybaitsConfig/SqlMapConfig.xml";
		// 得到配置文件流
		InputStream inputStream = Resources.getResourceAsStream(resource);

		// 创建会话工厂，传入mybatis的配置文件信息
		// 通过SqlSessionFactoryBuilder创建会话工厂SqlSessionFactory
		// 将SqlSessionFactoryBuilder当成一个工具类使用即可，不需要使用单例管理SqlSessionFactoryBuilder。
		sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

	}

	@Test
	public void testGetOrdersAndUser() throws Exception {
		SqlSession sqlSession = sqlSessionFactory.openSession();
		OrdersMapper mapper = sqlSession.getMapper(OrdersMapper.class);
		List<OrdersCustom> ordersAndUser = mapper.getOrdersAndUser();
		System.out.println(ordersAndUser);
	}

	@Test
	public void testGetOrdersAndUserResultMap() throws Exception {
		SqlSession sqlSession = sqlSessionFactory.openSession();
		OrdersMapper mapper = sqlSession.getMapper(OrdersMapper.class);
		List<Orders> ordersAndUser = mapper.getOrdersAndUserResultMap();
		System.out.println(ordersAndUser);
	}
	@Test
	public void findOrderAndOrderDetailResultMap() throws Exception {
		SqlSession sqlSession = sqlSessionFactory.openSession();
		OrdersMapper mapper = sqlSession.getMapper(OrdersMapper.class);
		List<Orders> ordersAndUser = mapper.findOrderAndOrderDetailResultMap();
		System.out.println(ordersAndUser);
	}
	@Test
	public void findUserAndItemsResutlMap() throws Exception {
		SqlSession sqlSession = sqlSessionFactory.openSession();
		OrdersMapper mapper = sqlSession.getMapper(OrdersMapper.class);
		List<User> ordersAndUser = mapper.findUserAndItemsResutlMap();
		System.out.println(ordersAndUser);
	}
 
}
