package cn.feng.mybatis.base.a_2.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import cn.feng.mybatis.base.a_2.test.entity.User;
import cn.feng.mybatis.base.a_4.mapper.UserMapper;

public class UserMapperTest {

	private static SqlSessionFactory sqlSessionFactory = null;

	/**
	 * 加载配置
	 */
	@Before
	public void config() throws IOException {
		// mybatis配置文件
		String resource = "mybaitsConfig/SqlMapConfig.xml";
		// 得到配置文件流
		InputStream inputStream = Resources.getResourceAsStream(resource);

		// 创建会话工厂，传入mybatis的配置文件信息
		// 通过SqlSessionFactoryBuilder创建会话工厂SqlSessionFactory
		// 将SqlSessionFactoryBuilder当成一个工具类使用即可，不需要使用单例管理SqlSessionFactoryBuilder。
		sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

	}

	@Test
	public void testGetUserById() throws Exception {
		SqlSession sqlSession = sqlSessionFactory.openSession();

		// 创建UserMapper对象，mybatis自动生成mapper代理对象
		UserMapper mapper = sqlSession.getMapper(UserMapper.class);
		
		// 调用userMapper的方法
		User user = mapper.getUserById(1);
		System.out.println(user);


	}
	@Test
	public void testgetUserByIdResultMap() throws Exception {
		SqlSession sqlSession = sqlSessionFactory.openSession();
		
		// 创建UserMapper对象，mybatis自动生成mapper代理对象
		UserMapper mapper = sqlSession.getMapper(UserMapper.class);
		
		// 调用userMapper的方法
		User user = mapper.getUserByIdResultMap(1);
		System.out.println(user);
		
		
	}

}
