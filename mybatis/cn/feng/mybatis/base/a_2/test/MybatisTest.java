package cn.feng.mybatis.base.a_2.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import cn.feng.mybatis.base.a_2.test.entity.User;

public class MybatisTest {
	private static SqlSession sqlSession = null;

	/**
	 * 加载配置
	 */
	@Before
	public void config() throws IOException {
		// mybatis配置文件
		String resource = "mybaitsConfig/SqlMapConfig.xml";
		// 得到配置文件流
		InputStream inputStream = Resources.getResourceAsStream(resource);

		// 创建会话工厂，传入mybatis的配置文件信息
		// 通过SqlSessionFactoryBuilder创建会话工厂SqlSessionFactory
		// 将SqlSessionFactoryBuilder当成一个工具类使用即可，不需要使用单例管理SqlSessionFactoryBuilder。
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder()
				.build(inputStream);

		// 通过工厂得到SqlSession
		// 通过SqlSessionFactory创建SqlSession，使用单例模式管理sqlSessionFactory（工厂一旦创建，使用一个实例）。
		// 将来mybatis和spring整合后，使用单例模式管理sqlSessionFactory。
		sqlSession = sqlSessionFactory.openSession();
			
		//SqlSession是一个面向用户（程序员）的接口。
		// SqlSession中提供了很多操作数据库的方法：如：selectOne(返回单个对象)、selectList（返回单个或多个对象）、。
		//
		// SqlSession是线程不安全的，在SqlSesion实现类中除了有接口中的方法（操作数据库的方法）还有数据域属性。
		//
		// SqlSession最佳应用场合在方法体内，定义成局部变量使用。
	}

	/**
	 * 错误: 密码错误: 方法名错误: 配置结果映射错误了resultMap:Could not find result map
	 * cn.feng.mybatis.a_2.test.entity.User
	 */
	@Test
	public void getUserByIdTest() throws IOException {
		// 通过SqlSession操作数据库
		// 第一个参数：映射文件中statement的id，等于=namespace+"."+statement的id
		// 第二个参数：指定和映射文件中所匹配的parameterType类型的参数
		// sqlSession.selectOne结果 是与映射文件中所匹配的resultType类型的对象
		// selectOne查询出一条记录
		// 表示查询出一条记录进行映射。如果使用selectOne可以实现使用selectList也可以实现（list中只有一个对象）。
		/*
		 * 如果使用selectOne报错 org.apache.ibatis.exceptions.TooManyResultsException:
		 * Expected one result (or null) to be returned by selectOne(), but
		 * found: 4
		 */
		User user = sqlSession.selectOne("test.getUserById", 1);

		System.out.println(user);

		// 释放资源
		sqlSession.close();
	}

	@Test
	public void getUserByNameTest() {
		// selectList表示查询出一个列表（多条记录）进行映射。如果使用selectList查询多条记录，不能使用selectOne。
		List<User> user = sqlSession.selectList("test.getUserByName", "王");
		System.out.println(user);
		// 释放资源
		sqlSession.close();
	}

	@Test
	public void insertUserTest() {
		// 插入用户对象
		User user = new User();
		user.setUsername("王小军");
		user.setBirthday(new Date());
		user.setSex("1");
		user.setAddress("河南郑州");

		sqlSession.insert("test.insertUser", user);
		// 提交事务
		sqlSession.commit();

		// 获取用户信息主键
		System.out.println(user.getId());
		// 关闭会话
		sqlSession.close();
	}

	@Test
	public void updateUserTest() {
		// 更新用户信息
		User user = new User();
		// 必须设置id
		user.setId(41);
		user.setUsername("王大军");
		user.setBirthday(new Date());
		user.setSex("2");
		user.setAddress("河南郑州");

		sqlSession.update("test.updateUser", user);

		// 提交事务
		sqlSession.commit();

		// 关闭会话
		sqlSession.close();
	}
	
	// 根据id删除 用户信息
	@Test
	public void deleteUserTest() throws IOException {
		

		// 传入id删除 用户
		sqlSession.delete("test.deleteUser", 39);

		// 提交事务
		sqlSession.commit();

		// 关闭会话
		sqlSession.close();

	}
	
}
