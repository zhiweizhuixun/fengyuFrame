package cn.feng.mybatis.base.a_2.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import cn.feng.mybatis.base.a_2.test.entity.User;
import cn.feng.mybatis.base.a_3.dao.UserDao;
import cn.feng.mybatis.base.a_3.dao.impl.UserDaoImpl;

public class UserDaoImplTest {
	private static SqlSessionFactory sqlSessionFactory = null;

	/**
	 * 加载配置
	 */
	@Before
	public void config() throws IOException {
		// mybatis配置文件
		String resource = "mybaitsConfig/SqlMapConfig.xml";
		// 得到配置文件流
		InputStream inputStream = Resources.getResourceAsStream(resource);

		// 创建会话工厂，传入mybatis的配置文件信息
		// 通过SqlSessionFactoryBuilder创建会话工厂SqlSessionFactory
		// 将SqlSessionFactoryBuilder当成一个工具类使用即可，不需要使用单例管理SqlSessionFactoryBuilder。
		 sqlSessionFactory = new SqlSessionFactoryBuilder()
				.build(inputStream);

	
	}


	@Test
	public void testGetUserById() throws Exception {
		UserDao userDao=new UserDaoImpl(sqlSessionFactory);
		User user = userDao.getUserById(10);
		System.out.println(user);
	
	}

	@Test
	public void testGetUserByName() {
		fail("Not yet implemented");
	}

	@Test
	public void testInsertUser() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteUser() {
		fail("Not yet implemented");
	}

}
