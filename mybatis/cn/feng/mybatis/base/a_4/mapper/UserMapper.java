package cn.feng.mybatis.base.a_4.mapper;

import cn.feng.mybatis.base.a_2.test.entity.User;

/**
 * 4.3.1思路（mapper代理开发规范）
 * 
 * 程序员还需要编写mapper.xml映射文件 程序员编写mapper接口需要遵循一些开发规范，mybatis可以自动生成mapper接口实现类代理对象。
 */
public interface UserMapper {
	// 根据id查询用户信息
	public User getUserById(int id) throws Exception;
	// 使用resultMap进行输出映射
	public User getUserByIdResultMap(int id) throws Exception;

}
/**
 * 
 开发规范： 1、在mapper.xml中namespace等于mapper接口地址
 * 2、mapper.java接口中的方法名和mapper.xml中statement的id一致
 * 
 * 3、mapper.java接口中的方法输入参数类型和mapper.xml中statement的parameterType指定的类型一致。
 * 
 * 4、mapper.java接口中的方法返回值类型和mapper.xml中statement的resultType指定的类型一致。
 */
/**
 * 4.3.6.1代理对象内部调用selectOne或selectList
 * 
 * 如果mapper方法返回单个pojo对象（非集合对象），代理对象内部通过selectOne查询数据库。
 * 
 * 如果mapper方法返回集合对象，代理对象内部通过selectList查询数据库。
 * 
 */
/**
 * 4.3.6.2mapper接口方法参数只能有一个是否影响系统 开发 mapper接口方法参数只能有一个，系统是否不利于扩展维护。
 * 
 * 系统 框架中，dao层的代码是被业务层公用的。 即使mapper接口只有一个参数，可以使用包装类型的pojo满足不同的业务方法的需求。
 * 
 * 注意：持久层方法的参数可以包装类型、map。。。，service方法中建议不要使用包装类型（不利于业务层的可扩展）。
 * 
 * 
 * */
