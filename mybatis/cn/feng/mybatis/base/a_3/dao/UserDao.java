package cn.feng.mybatis.base.a_3.dao;

import java.util.List;

import cn.feng.mybatis.base.a_2.test.entity.User;

/**
 * 4.2原始dao开发方法（程序员需要写dao接口和dao实现类） 
 * 4.2.1思路 程序员需要写dao接口和dao实现类。
 * 需要向dao实现类中注入SqlSessionFactory，在方法体内通过SqlSessionFactory创建SqlSession
 */
public interface UserDao {
	// 根据id查询用户信息
	public User getUserById(int id) throws Exception;

	// 根据用户名列查询用户列表
	public List<User> getUserByName(String name) throws Exception;

	// 添加用户信息
	public void insertUser(User user) throws Exception;

	// 删除用户信息
	public void deleteUser(int id) throws Exception;
}
/**
 *4.2.5总结原始 dao开发问题
1、dao接口实现类方法中存在大量模板方法，设想能否将这些代码提取出来，大大减轻程序员的工作量。

2、调用sqlsession方法时将statement的id硬编码了

3、调用sqlsession方法时传入的变量，由于sqlsession方法使用泛型，即使变量类型传入错误，在编译阶段也不报错，不利于程序员开发。
 */
