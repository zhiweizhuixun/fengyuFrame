package com.fengyu.data.element.config.tool;

import java.io.Serializable;
import java.util.List;

/**
 * 数据库配置信息
 * 
 * @author Hongten
 * @mail hongtenzone@foxmail.com
 * @create 2013-8-3
 */
public class DataBaseBO implements Serializable {
	private static final long serialVersionUID = 171777003280248377L;
	private final String SELECT_SQL_FIELD = " column_name as field,";
	private final String SELECT_SQL_TYPE = " data_type as type,";
	private final String SELECT_SQL_MEMO = " column_comment as memo,";
	private final String SELECT_SQL_MUNERIC_LENGTH = " numeric_precision as munericLength,";
	private final String SELECT_SQL_NUMERIC_SCALE = " numeric_scale as numericScale, ";
	private final String SELECT_SQL_ISNULLABLE = " is_nullable as isNullable,";
	private final String SELECT_SQL_EXTRA = " CASE WHEN extra = 'auto_increment' THEN 1 ELSE 0 END as extra,";
	private final String SELECT_SQL_ISDEFAULT = " column_default as isDefault,";
	private final String SELECT_SQL_CHARACTER_LENGTH = " character_maximum_length  AS characterLength ";
	/**
	 * 查询表结构sql
	 */
	private String selectSQL = "SELECT " + SELECT_SQL_FIELD + SELECT_SQL_TYPE + SELECT_SQL_MEMO
			+ SELECT_SQL_MUNERIC_LENGTH + SELECT_SQL_NUMERIC_SCALE + SELECT_SQL_ISNULLABLE + SELECT_SQL_EXTRA
			+ SELECT_SQL_ISDEFAULT + SELECT_SQL_CHARACTER_LENGTH
			+ " FROM Information_schema.columns WHERE  table_Name = ";
	/**
	 * 驱动名称
	 */
	private String driver;
	/**
	 * 数据库名称
	 */
	private String dbName;
	/**
	 * 数据库密码
	 */
	private String passwrod;
	/**
	 * 数据库用户名
	 */
	private String userName;
	/**
	 * 访问数据库的url
	 */
	private String url;
	/**
	 * 端口号
	 */
	private String port;
	/**
	 * ip地址
	 */
	private String ip;
	/**
	 * 数据类型：mysql， oracle等等
	 */
	private String dbType;

	/**
	 * 根据sql:show tables;查询出的数据库表名称
	 */
	private List<String> tables;
	/**
	 * 数据库表名称
	 */
	private String tableName;
	/**
	 * sql语句
	 */
	private String sql;

	/**
	 * 查询表结构sql
	 */
	public String getDriver() {
		return driver;
	}

	/**
	 * 查询表结构sql
	 */
	public void setDriver(String driver) {
		this.driver = driver;
	}

	/**
	 * 数据库名称
	 */
	public String getDbName() {
		return dbName;
	}

	/**
	 * 数据库名称
	 */
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	/**
	 * 数据库密码
	 */
	public String getPasswrod() {
		return passwrod;
	}

	/**
	 * 数据库密码
	 */
	public void setPasswrod(String passwrod) {
		this.passwrod = passwrod;
	}

	/**
	 * 数据库用户名
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * 数据库用户名
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * 访问数据库的url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * 访问数据库的url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * sql语句
	 */

	public String getSql() {
		return sql;
	}

	/**
	 * sql语句
	 */
	public void setSql(String sql) {
		this.sql = sql;
	}

	/**
	 * 端口号
	 */
	public String getPort() {
		return port;
	}

	/**
	 * 端口号
	 */
	public void setPort(String port) {
		this.port = port;
	}

	/**
	 * ip地址
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * ip地址
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * 数据类型：mysql， oracle等等
	 */
	public String getDbType() {
		return dbType;
	}

	/**
	 * 数据类型：mysql， oracle等等
	 */
	public void setDbType(String dbType) {
		this.dbType = dbType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * 根据sql:show tables;查询出的数据库表名称
	 */
	public List<String> getTables() {
		return tables;
	}

	/**
	 * 根据sql:show tables;查询出的数据库表名称
	 */
	public void setTables(List<String> tables) {
		this.tables = tables;
	}

	/**
	 * 数据库表名称
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * 数据库表名称
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * 查询表结构sql
	 */
	public String getSelectSQL() {
		return selectSQL;
	}

	/**
	 * 查询表结构sql
	 */
	public void setSelectSQL(String selectSQL) {
		this.selectSQL = selectSQL;
	}

	@Override
	public String toString() {
		return "DataBaseBO [driver=" + driver + ", dbName=" + dbName + ", passwrod=" + passwrod + ", userName="
				+ userName + ", url=" + url + ", port=" + port + ", ip=" + ip + ", dbType=" + dbType + ", tables="
				+ tables + ", tableName=" + tableName + "]";
	}
	

}
