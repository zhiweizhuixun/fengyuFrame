/**
 * 
 */
package com.fengyu.data.element.config.tool;

import java.io.Serializable;

/**
 * 数据库表结构情况BO
 * 
 * @author Hongten
 * @mail hongtenzone@foxmail.com
 * @create 2013-8-3
 */
public class TableDescBO implements Serializable {
	private static final long serialVersionUID = 6450523501528806316L;
	/**
	 * 数据库表中对应的字段名称
	 */
	private String field;
	/**
	 * 数据库表中对应字段的类型
	 */
	private String type;
	/**
	 * 数据库表中字段是否为空：YES/NO
	 */
	private String isNullable;
	/**
	 * 是否为主键：KEY，不是，则为空，null
	 */
	private String key;
	/**
	 * 字段的默认值
	 */
	private String isDefault;
	/**
	 * 额外的属性，如：auto_increment
	 */
	private String extra;
	/**
	 * 小数位数
	 */
	private String numericScale;
	/**
	 * 数字长度
	 */
	private String munericLength;

	/**
	 * 字符长度
	 */
	private String characterLength;
	/**
	 * 备注
	 */
	private String memo;

//	/**
//	 * 重写toStirng方法 主要是为了控制台输出
//	 */
//	public String toString() {
//		return " " + field + "   " + type + "    " + isNullable + "        " + key + "        " + isDefault + "      " + extra + "            "+ memo;
//	}

	
	/**数据库表中对应的字段名称
	 * @return
	 */
	public String getField() {
		return field;
	}

	@Override
	public String toString() {
		return "TableDescBO [field=" + field + ", type=" + type + ", isNullable=" + isNullable + ", key=" + key
				+ ", isDefault=" + isDefault + ", extra=" + extra + ", numericScale=" + numericScale
				+ ", munericLength=" + munericLength + ", characterLength=" + characterLength + ", memo=" + memo + "]";
	}

	/**数据库表中对应的字段名称
	 * @param field
	 */
	public void setField(String field) {
		this.field = field;
	}

	/**数据库表中对应字段的类型
	 * @return
	 */
	public String getType() {
		return type;
	}

	/**数据库表中对应字段的类型
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**数据库表中字段是否为空：YES/NO
	 * @return
	 */
	public String getIsNullable() {
		return isNullable;
	}

	/**数据库表中字段是否为空：YES/NO
	 * @param isNullable
	 */
	public void setIsNullable(String isNullable) {
		this.isNullable = isNullable;
	}

	/**是否为主键：KEY，不是，则为空，null
	 * @return
	 */
	public String getKey() {
		return key;
	}

	/**是否为主键：KEY，不是，则为空，null
	 * @param key
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/** 字段的默认值
	 * @return
	 */
	public String getIsDefault() {
		return isDefault;
	}

	/** 字段的默认值
	 * @param isDefault
	 */
	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}

	/**额外的属性，如：auto_increment
	 * @return
	 */
	public String getExtra() {
		return extra;
	}

	/**额外的属性，如：auto_increment
	 * @param extra
	 */
	public void setExtra(String extra) {
		this.extra = extra;
	}

	/**
	 * 小数位数
	 */
	public String getNumericScale() {
		return numericScale;
	}
	/**
	 * 小数位数
	 */
	public void setNumericScale(String numericScale) {
		this.numericScale = numericScale;
	}

	/**
	 * 数字长度
	 */
	public String getMunericLength() {
		return munericLength;
	}

	/**
	 * 数字长度
	 */
	public void setMunericLength(String munericLength) {
		this.munericLength = munericLength;
	}

	/**
	 * 字符长度
	 */
	public String getCharacterLength() {
		return characterLength;
	}

	/**
	 * 字符长度
	 */
	public void setCharacterLength(String characterLength) {
		this.characterLength = characterLength;
	}
	/**
	 * 备注
	 */
	public String getMemo() {
		return memo;
	}
	/**
	 * 备注
	 */
	public void setMemo(String memo) {
		this.memo = memo;
	}

}
