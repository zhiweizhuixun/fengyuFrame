package cn.feng.enhance.a3_IntroSpector;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.junit.Test;

/**
 * @author Administrator 内省 //"x"-->"X"-->"getX"-->MethodGetX-->
 */
public class IntroSpectorTest {

	@Test
	public void testJava7() {
		// <String> list = ["item"];
		// String item = list[0];
		// Set<String> set = {"item"};
		Map<String, Integer> map = null;

		String string = "a";
		switch (string) {
		case "a":

			break;

		default:
			break;
		}
	}

	public static void main(String args[]) throws Exception {

		// testIntroSpectorRead();
		// testIntroSpectorRead();
		testIntrospector();
		ReflectPoint pt1 = new ReflectPoint(3, 6);
		BeanUtils.setProperty(pt1, "birthday.time", "111");
		System.out.println(BeanUtils.getProperty(pt1, "birthday.time"));
		
		PropertyUtils.setProperty(pt1, "x", 9);
		System.out.println(PropertyUtils.getProperty(pt1, "x").getClass().getName());

	}

	private static void testIntrospector() throws IntrospectionException, IllegalAccessException,
			InvocationTargetException {
		ReflectPoint reflectPoint = new ReflectPoint(3, 6);
		// 获取属性名
		String propertiesName = "x";

		PropertyDescriptor propertyDescriptor = new PropertyDescriptor(propertiesName, reflectPoint.getClass());

		setProperties(reflectPoint, propertyDescriptor);
		// 获取读取方法
		getProperties(reflectPoint, propertyDescriptor);
	}

	private static void setProperties(ReflectPoint reflectPoint, PropertyDescriptor propertyDescriptor)
			throws IllegalAccessException, InvocationTargetException {
		// 获取设置方法
		Method writeMethod = propertyDescriptor.getWriteMethod();
		// 反射
		writeMethod.invoke(reflectPoint, 5);
	}

	private static Object getProperty(Object obj, String propertyName) throws IntrospectionException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
		PropertyDescriptor[] propertis = beanInfo.getPropertyDescriptors();
		Object retVal = null;
		for (PropertyDescriptor pd : propertis) {
			if (pd.getName().equals(propertyName)) {
				Method readMethod = pd.getReadMethod();
				retVal = readMethod.invoke(obj);
				break;
			}
		}

		return null;
	}

	private static Object getProperties(ReflectPoint reflectPoint, PropertyDescriptor propertyDescriptor)
			throws IllegalAccessException, InvocationTargetException {
		// 获取读取方法
		Method readMethod = propertyDescriptor.getReadMethod();
		// 反射
		Object retVal = readMethod.invoke(reflectPoint);
		System.out.println(retVal);
		return retVal;
	}

}

class ReflectPoint {
	private Date birthday = new Date();

	private int x;
	public int y;
	public String str1 = "ball";
	public String str2 = "basketball";
	public String str3 = "itcast";

	public ReflectPoint(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final ReflectPoint other = (ReflectPoint) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return str1 + ":" + str2 + ":" + str3;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

}
