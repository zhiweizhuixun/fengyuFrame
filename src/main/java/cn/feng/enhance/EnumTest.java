package cn.feng.enhance;

public class EnumTest {

	@SuppressWarnings("static-access")
	public static void main(String args[]) {
		WeekDay weekDay = WeekDay.MONDAY;

		System.out.println(weekDay.nextDay());
		Weekdays weekdays = Weekdays.FRI;
		System.out.println(weekdays);
		System.out.println(weekdays.ordinal());
		System.out.println(weekdays.name());
		System.out.println(weekdays.valueOf("SUN"));
		System.out.println(weekdays.values().length);

	}

	// @Test
	

	public enum Weekdays {

		SUN(0), MON(1), TUE, WED, THI, FRI, SAT;
		//枚举默认调用无参构造
		private Weekdays() {
				System.out.println("first");
		}
		
		private Weekdays(int day){
			System.out.println("second");
			
		}
		
	}
	
}

/**
 * @author Administrator 普通的java类模拟Enum
 */
abstract class WeekDay {

	private WeekDay() {
	}

	public static final WeekDay MONDAY = new WeekDay() {
		public WeekDay nextDay() {
			return SUN;

		}
	};
	public static final WeekDay SUN = new WeekDay() {
		public WeekDay nextDay() {
			return MONDAY;

		}
	};

	public abstract WeekDay nextDay();

	/*
	 * public WeekDay nextDay(){ if(this==MONDAY){ return SUN; }else { return
	 * MONDAY; }
	 * 
	 * }
	 */

	public String toString() {
		return this == MONDAY ? "MONDAY" : "SUN";
	}

}
