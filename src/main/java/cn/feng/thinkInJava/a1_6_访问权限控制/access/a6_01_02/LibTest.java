package cn.feng.thinkInJava.a1_6_访问权限控制.access.a6_01_02;
//: access/LibTest.java
// Uses the library.
import net.mindview.simple.*;

/**创建独一无二的包名
 * @author fengyu
 * @date  2015年8月9日
 */
public class LibTest {
  public static void main(String[] args) {
    Vector v = new Vector();
    List l = new List();
  }
} /* Output:
net.mindview.simple.Vector
net.mindview.simple.List
*///:~
