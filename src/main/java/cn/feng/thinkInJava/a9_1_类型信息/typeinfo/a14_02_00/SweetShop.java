package cn.feng.thinkInJava.a9_1_类型信息.typeinfo.a14_02_00;
//: typeinfo/SweetShop.java
// Examination of the way the class loader works.
import static net.mindview.util.Print.*;

class Candy {
  static { print("Loading Candy"); }
}

class Gum {
  static { print("Loading Gum"); }
}

class Cookie {
  static { print("Loading Cookie"); }
}

public class SweetShop {
  public static void main(String[] args) {	
    print("inside main");
    new Candy();//Class对象仅在需要的时候才被加载,static初始化实在类加载的时候进行的
    print("After creating Candy");
    try {
    	//对 Class.forName()的调用为了它产生的副作用:如果类没有加载就加载它,在加载的过程中static字句被执行
      Class.forName("cn.feng.thinkInJava.a9_1_类型信息.typeinfo.a14_02_00.Gum");
    } catch(ClassNotFoundException e) {
      print("Couldn't find Gum");
    }
    print("After Class.forName(\"Gum\")");
    new Cookie();
    print("After creating Cookie");
  }
} /* Output:
inside main
Loading Candy
After creating Candy
Loading Gum
After Class.forName("Gum")
Loading Cookie
After creating Cookie
*///:~
