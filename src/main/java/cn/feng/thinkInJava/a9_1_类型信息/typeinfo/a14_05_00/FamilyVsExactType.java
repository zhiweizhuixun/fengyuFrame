//: typeinfo/FamilyVsExactType.java
// The difference between instanceof and class
package cn.feng.thinkInJava.a9_1_类型信息.typeinfo.a14_05_00;

import static net.mindview.util.Print.*;

/**instanceof与Class的等价性
 * @author fengyu
 * @date  2015年8月9日
 */
class Base
{
}

class Derived extends Base
{
}

public class FamilyVsExactType
{
	static void test(Object x)
	{
		print("Testing x of type "+x.getClass());
		print("x instanceof Base "+(x instanceof Base));//instanceof保持了类型的概念,它指的是"你是这个类吗,或者你是这类的派生类吗?"
		print("x instanceof Derived "+(x instanceof Derived));
		print("Base.isInstance(x) "+Base.class.isInstance(x));
		print("Derived.isInstance(x) "+Derived.class.isInstance(x));
		print("x.getClass() == Base.class "+(x.getClass()==Base.class));//==比较实际的Class对象没有考虑继承--他或者是这个确切类型,或者不是
		print("x.getClass() == Derived.class "+(x.getClass()==Derived.class));
		print("x.getClass().equals(Base.class)) "+(x.getClass().equals(Base.class)));
		print("x.getClass().equals(Derived.class)) "+(x.getClass().equals(Derived.class)));
	}
	
	public static void main(String[] args)
	{
		test(new Base());
		test(new Derived());
	}
} /* Output:
Testing x of type class typeinfo.Base
x instanceof Base true
x instanceof Derived false
Base.isInstance(x) true
Derived.isInstance(x) false
x.getClass() == Base.class true
x.getClass() == Derived.class false
x.getClass().equals(Base.class)) true
x.getClass().equals(Derived.class)) false
Testing x of type class typeinfo.Derived
x instanceof Base true
x instanceof Derived true
Base.isInstance(x) true
Derived.isInstance(x) true
x.getClass() == Base.class false
x.getClass() == Derived.class true
x.getClass().equals(Base.class)) false
x.getClass().equals(Derived.class)) true
*///:~
