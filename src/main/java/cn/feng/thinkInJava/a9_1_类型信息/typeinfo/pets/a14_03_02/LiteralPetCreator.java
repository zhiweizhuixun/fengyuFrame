//: typeinfo/pets/LiteralPetCreator.java
// Using class literals.
package cn.feng.thinkInJava.a9_1_类型信息.typeinfo.pets.a14_03_02;

import java.util.*;

import cn.feng.thinkInJava.a9_1_类型信息.typeinfo.pets.Cat;
import cn.feng.thinkInJava.a9_1_类型信息.typeinfo.pets.Cymric;
import cn.feng.thinkInJava.a9_1_类型信息.typeinfo.pets.Dog;
import cn.feng.thinkInJava.a9_1_类型信息.typeinfo.pets.EgyptianMau;
import cn.feng.thinkInJava.a9_1_类型信息.typeinfo.pets.Hamster;
import cn.feng.thinkInJava.a9_1_类型信息.typeinfo.pets.Manx;
import cn.feng.thinkInJava.a9_1_类型信息.typeinfo.pets.Mouse;
import cn.feng.thinkInJava.a9_1_类型信息.typeinfo.pets.Mutt;
import cn.feng.thinkInJava.a9_1_类型信息.typeinfo.pets.Pet;
import cn.feng.thinkInJava.a9_1_类型信息.typeinfo.pets.Pug;
import cn.feng.thinkInJava.a9_1_类型信息.typeinfo.pets.Rat;
import cn.feng.thinkInJava.a9_1_类型信息.typeinfo.pets.Rodent;
import cn.feng.thinkInJava.a9_1_类型信息.typeinfo.pets.a14_03_00.PetCreator;

/**使用类字面常量
 * @author fengyu
 * @date  2015年8月9日
 */
public class LiteralPetCreator extends PetCreator
{
	// No try block needed.
	@SuppressWarnings("unchecked")
	public static final List<Class<? extends Pet>> allTypes=Collections.unmodifiableList(Arrays.asList(
			Pet.class,Dog.class,Cat.class,Rodent.class,Mutt.class,
			Pug.class,EgyptianMau.class,Manx.class,Cymric.class,
			Rat.class,Mouse.class,Hamster.class));
	// Types for random creation:
	private static final List<Class<? extends Pet>> types=allTypes.subList(allTypes.indexOf(Mutt.class),allTypes.size());
	
	public List<Class<? extends Pet>> types()
	{
		return types;
	}
	
	public static void main(String[] args)
	{
		System.out.println(types);
	}
} /*
 * Output: [class typeinfo.pets.Mutt, class typeinfo.pets.Pug, class
 * typeinfo.pets.EgyptianMau, class typeinfo.pets.Manx, class
 * typeinfo.pets.Cymric, class typeinfo.pets.Rat, class typeinfo.pets.Mouse,
 * class typeinfo.pets.Hamster]
 */// :~
