//: typeinfo/pets/PetCreator.java
// Creates random sequences of Pets.
package cn.feng.thinkInJava.a9_1_类型信息.typeinfo.pets.a14_03_00;

import java.util.*;

import cn.feng.thinkInJava.a9_1_类型信息.typeinfo.pets.Pet;

/**
 * 类型转换前先做检查
 * 
 * @author fengyu
 * @date 2015年8月9日
 */
public abstract class PetCreator
{
	private Random rand=new Random(47);
	
	// The List of the different types of Pet to create:
	public abstract List<Class<? extends Pet>> types();
	
	public Pet randomPet()
	{ // Create one random Pet
		int n=rand.nextInt(types().size());
		try
		{
			return types().get(n).newInstance();
		}
		catch(InstantiationException e)
		{
			throw new RuntimeException(e);
		}
		catch(IllegalAccessException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	public Pet[] createArray(int size)
	{
		Pet[] result=new Pet[size];
		for(int i=0;i<size;i++)
			result[i]=randomPet();
		return result;
	}
	
	public ArrayList<Pet> arrayList(int size)
	{
		ArrayList<Pet> result=new ArrayList<Pet>();
		Collections.addAll(result,createArray(size));
		return result;
	}
} // /:~
