package cn.feng.thinkInJava.a9_1_类型信息.typeinfo.a14_08_00;

//: typeinfo/Person.java
// A class with a Null Object.
import net.mindview.util.*;

/**空对象
 * @author fengyu
 * @date  2015年8月9日
 */
class Person
{
	public final String first;
	public final String last;
	public final String address;
	
	// etc.
	public Person(String first,String last,String address)
	{
		this.first=first;
		this.last=last;
		this.address=address;
	}
	
	public String toString()
	{
		return "Person: "+first+" "+last+" "+address;
	}
	public static class NullPerson extends Person implements Null
	{
		private NullPerson()
		{
			super("None","None","None");
		}
		
		public String toString()
		{
			return "NullPerson";
		}
	}
	public static final Person NULL=new NullPerson();
} // /:~
