package cn.feng.thinkInJava.a9_3_容器.containers.a17_10_00;


/**性能测试框架
 * @author fengyu
 * @date  2015年8月9日
 * @param <C>
 */
public abstract class Test<C> {
  String name;
  public Test(String name) { this.name = name; }
  // Override this method for different tests.
  // Returns actual number of repetitions of test.
  abstract int test(C container, TestParam tp);
} ///:~
