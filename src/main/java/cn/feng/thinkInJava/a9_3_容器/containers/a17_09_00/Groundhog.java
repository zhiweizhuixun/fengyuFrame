package cn.feng.thinkInJava.a9_3_容器.containers.a17_09_00;
//: containers/Groundhog.java
// Looks plausible, but doesn't work as a HashMap key.

/**散列码和散列
 * @author fengyu
 * @date  2015年8月9日
 */
public class Groundhog {
  protected int number;
  public Groundhog(int n) { number = n; }
  public String toString() {
    return "Groundhog #" + number;
  }
} ///:~
