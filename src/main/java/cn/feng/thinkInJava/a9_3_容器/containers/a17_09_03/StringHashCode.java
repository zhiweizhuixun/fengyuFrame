package cn.feng.thinkInJava.a9_3_容器.containers.a17_09_03;
//: containers/StringHashCode.java

/**覆盖hashCode()
 * @author fengyu
 * @date  2015年8月9日
 */
public class StringHashCode {
  public static void main(String[] args) {
    String[] hellos = "Hello Hello".split(" ");
    System.out.println(hellos[0].hashCode());
    System.out.println(hellos[1].hashCode());
  }
} /* Output: (Sample)
69609650
69609650
*///:~
