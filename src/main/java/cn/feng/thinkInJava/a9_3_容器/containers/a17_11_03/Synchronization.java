package cn.feng.thinkInJava.a9_3_容器.containers.a17_11_03;
//: containers/Synchronization.java
// Using the Collections.synchronized methods.
import java.util.*;

/**设定Collection或Map为 同步控制
 * @author fengyu
 * @date  2015年8月9日
 */
public class Synchronization {
  public static void main(String[] args) {
    Collection<String> c =
      Collections.synchronizedCollection(
        new ArrayList<String>());
    List<String> list = Collections.synchronizedList(
      new ArrayList<String>());
    Set<String> s = Collections.synchronizedSet(
      new HashSet<String>());
    Set<String> ss = Collections.synchronizedSortedSet(
      new TreeSet<String>());
    Map<String,String> m = Collections.synchronizedMap(
      new HashMap<String,String>());
    Map<String,String> sm =
      Collections.synchronizedSortedMap(
        new TreeMap<String,String>());
  }
} ///:~
