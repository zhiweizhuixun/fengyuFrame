//: polymorphism/shape/Circle.java
package cn.feng.thinkInJava.a6_1_多态.polymorphism.shape;
import static net.mindview.util.Print.*;

public class Circle extends Shape {
  public void draw() { print("Circle.draw()"); }
  public void erase() { print("Circle.erase()"); }
} ///:~
