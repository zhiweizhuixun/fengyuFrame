//: polymorphism/PrivateOverride.java
// Trying to override a private method.
package cn.feng.thinkInJava.a6_1_多态.polymorphism;

import static net.mindview.util.Print.*;

/**
 * 缺陷:"覆盖"私有方法
 * 建议:在导出类中的,对于基类中的private方法,最好采用不同方法名字
 */
public class A8_2_4_1_PrivateOverride {
	// 因为父类中 private 被自动认为final不会被导出类覆盖,是屏蔽的,基类中的f()方法在子类是不可见的,因此不会被覆盖
	private void f() {
		print("private f()");
	}

	public static void main(String[] args) {
		A8_2_4_1_PrivateOverride po = new Derived();
		po.f();
	}
}

class Derived extends A8_2_4_1_PrivateOverride {
	public void f() {
		print("public f()");
	}
} /*
 * Output: private f()
 */// :~
