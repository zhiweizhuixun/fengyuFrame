package cn.feng.thinkInJava.a6_1_多态.polymorphism;

//: polymorphism/CovariantReturn.java

class Grain {
	public String toString() {
		return "Grain";
	}
}

class Wheat extends Grain {
	public String toString() {
		return "Wheat";
	}
}

class Mill {
	Grain process() {
		return new Grain();
	}
}

class WheatMill extends Mill {
	/**
	 * 协变返回:表示在导出类中被覆盖的方法可以返回基类方法的返回类型的某种导出类型;
	 */
	Wheat process() {
		return new Wheat();
	}
}

/**
 * 协变返回:表示在导出类中被覆盖的方法可以返回基类方法的返回类型的某种导出类型;
 */
public class A8_4_0_1_CovariantReturn {
	public static void main(String[] args) {
		Mill m = new Mill();
		Grain g = m.process();
		System.out.println(g);
		m = new WheatMill();
		g = m.process();
		System.out.println(g);
	}
} /*
 * Output: Grain Wheat
 */// :~
