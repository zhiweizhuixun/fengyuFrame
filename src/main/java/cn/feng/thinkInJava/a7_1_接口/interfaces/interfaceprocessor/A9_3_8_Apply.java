//: interfaces/interfaceprocessor/Apply.java
package cn.feng.thinkInJava.a7_1_接口.interfaces.interfaceprocessor;
import static net.mindview.util.Print.*;

public class A9_3_8_Apply {
  public static void process(A9_3_7_Processor p, Object s) {
    print("Using Processor " + p.name());
    print(p.process(s));
  }
} ///:~
