package cn.feng.thinkInJava.a7_1_接口.interfaces;

//: interfaces/RandomWords.java
// Implementing an interface to conform to a method.
import java.nio.*;
import java.util.*;

/**
 * 接口最吸引人的原因就是允许同一个接口具有多个不同的具体实现 ,
 * 在简单的情况下接口的体现形式通常是一个接受接口类型的方法,
 * 而该接口的实现和向该方法传递的对象则取决于方法的使用者
 * 
 * @author fengyu
 * @date 2015年8月8日
 */
public class A9_6_00_RandomWords implements Readable {
	private static Random rand = new Random(47);
	private static final char[] capitals = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
	private static final char[] lowers = "abcdefghijklmnopqrstuvwxyz".toCharArray();
	private static final char[] vowels = "aeiou".toCharArray();
	private int count;

	public A9_6_00_RandomWords(int count) {
		this.count = count;
	}

	public int read(CharBuffer cb) {
		if (count-- == 0)
			return -1; // Indicates end of input
		cb.append(capitals[rand.nextInt(capitals.length)]);
		for (int i = 0; i < 4; i++) {
			cb.append(vowels[rand.nextInt(vowels.length)]);
			cb.append(lowers[rand.nextInt(lowers.length)]);
		}
		cb.append(" ");
		return 10; // Number of characters appended
	}

	public static void main(String[] args) {
		Scanner s = new Scanner(new A9_6_00_RandomWords(10));
		while (s.hasNext())
			System.out.println(s.next());
	}
} /*
 * Output: Yazeruyac Fowenucor Goeazimom Raeuuacio Nuoadesiw Hageaikux Ruqicibui
 * Numasetih Kuuuuozog Waqizeyoy
 */// :~
