package cn.feng.thinkInJava.a7_1_接口.interfaces;
//: interfaces/TestRandVals.java
import static net.mindview.util.Print.*;

public class A9_7_01_TestRandVals {
  /**初始化接口中的域
 *@author fengyu
 * @date  2015年8月8日
 * @param args
 */
public static void main(String[] args) {
    print(RandVals.RANDOM_INT);
    print(RandVals.RANDOM_LONG);
    print(RandVals.RANDOM_FLOAT);
    print(RandVals.RANDOM_DOUBLE);
  }
} /* Output:
8
-32032247016559954
-8.5939291E18
5.779976127815049
*///:~
