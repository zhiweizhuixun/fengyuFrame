//: interfaces/interfaceprocessor/FilterProcessor.java
package cn.feng.thinkInJava.a7_1_接口.interfaces.interfaceprocessor;
import cn.feng.thinkInJava.a7_1_接口.interfaces.filters.A9_3_6_BandPass;
import cn.feng.thinkInJava.a7_1_接口.interfaces.filters.A9_3_3_Filter;
import cn.feng.thinkInJava.a7_1_接口.interfaces.filters.A9_3_5_HighPass;
import cn.feng.thinkInJava.a7_1_接口.interfaces.filters.A9_3_4_LowPass;
import cn.feng.thinkInJava.a7_1_接口.interfaces.filters.A9_3_2_Waveform;

class FilterAdapter implements A9_3_7_Processor {
  A9_3_3_Filter A9_3_3_Filter;
  public FilterAdapter(A9_3_3_Filter A9_3_3_Filter) {
    this.A9_3_3_Filter = A9_3_3_Filter;
  }
  public String name() { return A9_3_3_Filter.name(); }
  public A9_3_2_Waveform process(Object input) {
    return A9_3_3_Filter.process((A9_3_2_Waveform)input);
  }
}	

public class A9_3_10_FilterProcessor {
  public static void main(String[] args) {
    A9_3_2_Waveform w = new A9_3_2_Waveform();
    A9_3_8_Apply.process(new FilterAdapter(new A9_3_4_LowPass(1.0)), w);
    A9_3_8_Apply.process(new FilterAdapter(new A9_3_5_HighPass(2.0)), w);
    A9_3_8_Apply.process(
      new FilterAdapter(new A9_3_6_BandPass(3.0, 4.0)), w);
  }
} /* Output:
Using Processor LowPass
Waveform 0
Using Processor HighPass
Waveform 0
Using Processor BandPass
Waveform 0
*///:~
