//: interfaces/interfaceprocessor/StringProcessor.java
package cn.feng.thinkInJava.a7_1_接口.interfaces.interfaceprocessor;
import java.util.*;

public abstract class A9_3_9_StringProcessor implements A9_3_7_Processor{
  public String name() {
    return getClass().getSimpleName();
  }
  public abstract String process(Object input);
  public static String s =
    "If she weighs the same as a duck, she's made of wood";
  public static void main(String[] args) {
    A9_3_8_Apply.process(new Upcase(), s);
    A9_3_8_Apply.process(new Downcase(), s);
    A9_3_8_Apply.process(new Splitter(), s);
  }
}	

class Upcase extends A9_3_9_StringProcessor {
  public String process(Object input) { // Covariant return
    return ((String)input).toUpperCase();
  }
}

class Downcase extends A9_3_9_StringProcessor {
  public String process(Object input) {
    return ((String)input).toLowerCase();
  }
}

class Splitter extends A9_3_9_StringProcessor {
  public String process(Object input) {
    return Arrays.toString(((String)input).split(" "));
  }	
} /* Output:
Using Processor Upcase
IF SHE WEIGHS THE SAME AS A DUCK, SHE'S MADE OF WOOD
Using Processor Downcase
if she weighs the same as a duck, she's made of wood
Using Processor Splitter
[If, she, weighs, the, same, as, a, duck,, she's, made, of, wood]
*///:~
