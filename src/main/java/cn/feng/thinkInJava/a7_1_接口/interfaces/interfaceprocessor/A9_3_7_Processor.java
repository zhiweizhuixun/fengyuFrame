//: interfaces/interfaceprocessor/Processor.java
package cn.feng.thinkInJava.a7_1_接口.interfaces.interfaceprocessor;

public interface A9_3_7_Processor {
  String name();
  Object process(Object input);
} ///:~
