//: interfaces/filters/Filter.java
package cn.feng.thinkInJava.a7_1_接口.interfaces.filters;

public class A9_3_3_Filter {
  public String name() {
    return getClass().getSimpleName();
  }
  public A9_3_2_Waveform process(A9_3_2_Waveform input) { return input; }
} ///:~
