//: interfaces/filters/BandPass.java
package cn.feng.thinkInJava.a7_1_接口.interfaces.filters;

public class A9_3_6_BandPass extends A9_3_3_Filter {
  double lowCutoff, highCutoff;
  public A9_3_6_BandPass(double lowCut, double highCut) {
    lowCutoff = lowCut;
    highCutoff = highCut;
  }
  public A9_3_2_Waveform process(A9_3_2_Waveform input) { return input; }
} ///:~
