package cn.feng.thinkInJava.a7_1_接口.interfaces;
//: interfaces/Adventure.java
// Multiple interfaces.

interface CanFight {
  void fight();
}

interface CanSwim {
  void swim();
}

interface CanFly {
  void fly();
}

class ActionCharacter {
  public void fight() {}
}	

/**将一个具体类和多个接口组合在一起时候,这个类必须放在前面,后面才跟着接口(否则编译器会报错)
 * @author fengyu
 * @date  2015年8月7日
 */
class Hero extends ActionCharacter
    implements CanFight, CanSwim, CanFly {
  public void swim() {}
  public void fly() {}
}

/**这个例子展示使用接口的核心原因:为了能够向上转型为多个基类型(以及由此带来的灵活性)
 * @author fengyu
 * @date  2015年8月7日
 */
public class A9_4_01_Adventure {
  public static void t(CanFight x) { x.fight(); }
  public static void u(CanSwim x) { x.swim(); }
  public static void v(CanFly x) { x.fly(); }
  public static void w(ActionCharacter x) { x.fight(); }
  public static void main(String[] args) {
    Hero h = new Hero();
    t(h); // Treat it as a CanFight
    u(h); // Treat it as a CanSwim
    v(h); // Treat it as a CanFly
    w(h); // Treat it as an ActionCharacter
  }
} ///:~
