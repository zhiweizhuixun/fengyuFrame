//: interfaces/filters/LowPass.java
package cn.feng.thinkInJava.a7_1_接口.interfaces.filters;

public class A9_3_4_LowPass extends A9_3_3_Filter {
  double cutoff;
  public A9_3_4_LowPass(double cutoff) { this.cutoff = cutoff; }
  public A9_3_2_Waveform process(A9_3_2_Waveform input) {
    return input; // Dummy processing
  }
} ///:~
