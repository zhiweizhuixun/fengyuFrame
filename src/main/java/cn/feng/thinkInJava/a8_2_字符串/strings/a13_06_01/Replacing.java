package cn.feng.thinkInJava.a8_2_字符串.strings.a13_06_01;

//: strings/Replacing.java
import static net.mindview.util.Print.*;

/**最后一个正则表达式工具是替换
 * @author fengyu
 * @date  2015年8月12日
 */
public class Replacing
{
	static String s=Splitting.knights;
	
	public static void main(String[] args)
	{
		print(s.replaceFirst("f\\w+","located"));//替换以字母f开头,后面跟着一个或多个字母
		print(s.replaceAll("shrubbery|tree|herring","banana"));
	}
} /* Output:
Then, when you have located the shrubbery, you must cut down the mightiest tree in the forest... with... a herring!
Then, when you have found the banana, you must cut down the mightiest banana in the forest... with... a banana!
*///:~
