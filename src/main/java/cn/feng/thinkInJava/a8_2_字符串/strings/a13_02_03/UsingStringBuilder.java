package cn.feng.thinkInJava.a8_2_字符串.strings.a13_02_03;

//: strings/UsingStringBuilder.java
import java.util.*;

public class UsingStringBuilder
{
	public static Random rand=new Random(47);
	
	public String toString()
	{
		StringBuilder result=new StringBuilder("[");
		for(int i=0;i<25;i++)
		{
			result.append(rand.nextInt(100));
			result.append(", ");
		}
		result.delete(result.length()-2,result.length());// 删除最后一个逗号与空格,以便添加右括号
		result.append("]");
		// result.append(a+"]"+c);这个会使编译器掉入陷阱,从而会在另外的创建一个StringBuilder对象处理括号内的字符串操作
		// 最终结果用append()语句拼接起来,
		return result.toString();
	}
	
	public static void main(String[] args)
	{
		UsingStringBuilder usb=new UsingStringBuilder();
		System.out.println(usb);
	}
} /*
	 * Output: [58, 55, 93, 61, 61, 29, 68, 0, 22, 7, 88, 28, 51, 89, 9, 78, 98,
	 * 61, 20, 58, 16, 40, 11, 22, 4]
	 */// :~
