package cn.feng.thinkInJava.a8_2_字符串.strings.a13_06_10;
//: strings/JGrep.java
// A very simple version of the "grep" program.
// {Args: JGrep.java "\\b[Ssct]\\w+"}
import java.util.regex.*;
import net.mindview.util.*;

/**正则表达式与java I/O
 * @author fengyu
 * @date  2015年8月8日
 */
public class JGrep {
  public static void main(String[] args) throws Exception {
	  args=new String[]{" JGrep.java ","\\b[Ssct]\\w+"};//以Ssct开头的单词
    if(args.length < 2) {
      System.out.println("Usage: java JGrep file regex");
      System.exit(0);
    }
    Pattern p = Pattern.compile(args[1]);
    // Iterate through the lines of the input file:
    int index = 0;
    Matcher m = p.matcher("");//在循环外创建一个空的Matcher对象
    for(String line : new TextFile(args[0])) {
    	 //然后用reset()方法每次为 Matcher加载一行输入,这种处理会有一定的性能优化
      m.reset(line);
      //搜索结果
      while(m.find())
        System.out.println(index++ + ": " +
          m.group() + ": " + m.start());
    }
  }
} /* Output: (Sample)
0: strings: 4
1: simple: 10
2: the: 28
3: Ssct: 26
4: class: 7
5: static: 9
6: String: 26
7: throws: 41
8: System: 6
9: System: 6
10: compile: 24
11: through: 15
12: the: 23
13: the: 36
14: String: 8
15: System: 8
16: start: 31
*///:~
