package cn.feng.thinkInJava.a8_2_字符串.strings.a13_07_00;

//: strings/BetterRead.java
import java.util.*;

public class BetterRead
{
	public static void main(String[] args)
	{	//Scanner构造器接受任何输入对象,包括File对象,InputStream,String或者Readable对象
		//Scanner,所有的输入分词以及翻译的操作都隐藏在不同类型的next方法中,普通的next方法返回下一个String,所有的基本类型(除了char之外)都有对应的next方法
		//包括BigDecimal和BigInteger,hasnext()方法判断是否是所需要的类型
		Scanner stdin=new Scanner(SimpleRead.input);
		
		System.out.println("What is your name?");
		String name=stdin.nextLine();
		System.out.println(name);
		System.out.println("How old are you? What is your favorite double?");
		System.out.println("(input: <age> <double>)");
		int age=stdin.nextInt();
		double favorite=stdin.nextDouble();
		System.out.println(age);
		System.out.println(favorite);
		System.out.format("Hi %s.\n",name);
		System.out.format("In 5 years you will be %d.\n",age+5);
		System.out.format("My favorite double is %f.",favorite/2);
	}
} /* Output:
What is your name?
Sir Robin of Camelot
How old are you? What is your favorite double?
(input: <age> <double>)
22
1.61803
Hi Sir Robin of Camelot.
In 5 years you will be 27.
My favorite double is 0.809015.
*///:~
