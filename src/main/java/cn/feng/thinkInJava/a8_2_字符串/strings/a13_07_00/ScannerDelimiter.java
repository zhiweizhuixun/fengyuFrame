package cn.feng.thinkInJava.a8_2_字符串.strings.a13_07_00;

//: strings/ScannerDelimiter.java
import java.util.*;
//定界符
public class ScannerDelimiter
{// ,默认情况下Scanner根据空白字符对输入进行分词,也可以自己指定正则自己所需的定界符
	public static void main(String[] args)
	{	//这个例子使用逗号(包括前后任意的空白字符串)作为定界符
		Scanner scanner=new Scanner("12, 42, 78, 99, 42");
		scanner.useDelimiter("\\s*,\\s*");
		while(scanner.hasNextInt())
			System.out.println(scanner.nextInt());
	}
} /* Output:
12
42
78
99
42
*///:~
