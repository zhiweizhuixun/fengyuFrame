package cn.feng.thinkInJava.a8_2_字符串.strings.a13_06_08;

//: strings/TheReplacements.java
import java.util.regex.*;
import net.mindview.util.*;
import static net.mindview.util.Print.*;

/*! Here's a block of text to use as input to
    the regular expression matcher. Note that we'll
    first extract the block of text by looking for
    the special delimiters, then process the
    extracted block. !*/
/**
 * 替换操作
 * 
 * @author fengyu
 * @date 2015年8月8日
 */
public class TheReplacements
{
	public static void main(String[] args) throws Exception
	{
		String s=TextFile.read("TheReplacements.java");
		// Match the specially commented block of text above:
		Matcher mInput=Pattern.compile("/\\*!(.*)!\\*/",Pattern.DOTALL).matcher(s);
		if(mInput.find())
			s=mInput.group(1); // Captured by parentheses
		// Replace two or more spaces with a single space:
		s=s.replaceAll(" {2,}"," ");
		// Replace one or more spaces at the beginning of each
		// line with no spaces. Must enable MULTILINE mode:
		
		s=s.replaceAll("(?m)^ +","");
		print(s);
		//替换第一个成功匹配的
		s=s.replaceFirst("[aeiou]","(VOWEL1)");
		StringBuffer sbuf=new StringBuffer();
		Pattern p=Pattern.compile("[aeiou]");
		Matcher m=p.matcher(s);
		// Process the find information as you
		// perform the replacements:
		while(m.find())
			//执行渐进式替换,,而不是像replaceAll和replaceFirst那样只替换全部匹配或第一个匹配,这是一个很重要的方法,它允许你调用其他方法来生成或处理replacements
			//使你能够以编程的方式将目标分割成组,从而具备强大的替换功能
			m.appendReplacement(sbuf,m.group().toUpperCase());
		// Put in the remainder of the text:在执行一次或多次appendReplacement()之后,调用此方法可以将输入字符串余下的部分复制到sbuf中
		m.appendTail(sbuf);
		print(sbuf);
	}
} /* Output:
Here's a block of text to use as input to
the regular expression matcher. Note that we'll
first extract the block of text by looking for
the special delimiters, then process the
extracted block.
H(VOWEL1)rE's A blOck Of tExt tO UsE As InpUt tO
thE rEgUlAr ExprEssIOn mAtchEr. NOtE thAt wE'll
fIrst ExtrAct thE blOck Of tExt by lOOkIng fOr
thE spEcIAl dElImItErs, thEn prOcEss thE
ExtrActEd blOck.
*///:~
