package cn.feng.thinkInJava.a8_2_字符串.strings.a13_06_00;
//: strings/IntegerMatch.java

/**
 * 正则表达式
 * 
 * @author fengyu
 * @date 2015年8月8日
 */
public class IntegerMatch
{
	public static void main(String[] args)
	{ // -?\\d+表示可能有一个负号,后面跟着一位或多位数字
		System.out.println("-1234".matches("-?\\d+"));// -?表示一个有可能一个负号在前面,\\表示插入一个正则表达式的反斜线,所以其后面的字符具有特殊意思
														// \\d表示一个数字,"+"表示一个或多个之前的表达式
		System.out.println("5678".matches("-?\\d+"));
		System.out.println("+911".matches("-?\\d+"));
		// |表示或操作,表示可能以一个加号或减号开头,括号表示分组的效果,
		// (-|\\+)?表示字符串的起始字符可能是一个-或+,否者二者都没有(因为后面跟着?修饰符)
		// 因为字符+在正则表达式有特殊意义,所以必须用\\将其转义,使之成为表达式中的额一个普通字符
		System.out.println("+911".matches("(-|\\+)?\\d+"));
	}
} /*
	 * Output: 
	 * 	true 
	 * 	true
	 *  false
	 *  true
	 */// :~
