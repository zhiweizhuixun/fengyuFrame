package cn.feng.thinkInJava.a8_2_字符串.strings.a13_03_00;

//: strings/InfiniteRecursion.java
// Accidental recursion.
// {RunByHand}
import java.util.*;

/**
 * 无意识递归
 * 
 * @author fengyu
 * @date 2015年8月12日
 */// 无限递归,导致StackOverflowError内存溢出错误
public class InfiniteRecursion
{
	public String toString()
	{
		// 这里发生自动类型转换,由于InfiniteRecursion类型转换成String类型.因为编译器看到一个String对象后
		// 面跟着一个"+",而在后面的对象不是String,于是编译器试着将this转成一个String,通过调用this上的toString方法,于是
		// 发生了递归调用,导致无限递归.从而发生内存溢出错误
		
		// return " InfiniteRecursion address: "+this+"\n";
		// 如果真的要打印出对象的内存地址,应该调用Object.toString()方法,这个才是负责此任务的方法,所以,不该使用this,
		// 而是应该调用super.toString()方法
		return " InfiniteRecursion address: "+super.toString()+"\n";
	}
	
	public static void main(String[] args)
	{
		List<InfiniteRecursion> v=new ArrayList<InfiniteRecursion>();
		for(int i=0;i<10;i++)
			v.add(new InfiniteRecursion());
		System.out.println(v);
	}
} /// :~
