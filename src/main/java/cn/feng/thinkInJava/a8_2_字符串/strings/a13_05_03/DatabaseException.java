package cn.feng.thinkInJava.a8_2_字符串.strings.a13_05_03;

//: strings/DatabaseException.java
/**
 * String.format()
 * 
 * @author fengyu
 * @date 2015年8月8日
 */
public class DatabaseException extends Exception
{
	public DatabaseException(int transactionID,int queryID,String message)
	{ // String.format()静态方法,返回一个String对象,内部也是一个Formatter对象,只需要format()方法一次时候, String.format()方法用起来方便
		super(String.format("(t%d, q%d) %s",transactionID,queryID,message));
	}
	
	public static void main(String[] args)
	{
		try
		{
			throw new DatabaseException(3,7,"Write failed");
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
} /*
	 * Output: DatabaseException: (t3, q7) Write failed
	 */// :~
