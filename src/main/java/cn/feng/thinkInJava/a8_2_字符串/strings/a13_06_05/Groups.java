package cn.feng.thinkInJava.a8_2_字符串.strings.a13_06_05;

//: strings/Groups.java
import java.util.regex.*;
import static net.mindview.util.Print.*;

/**组	
 * 	组的概念，这个概念很重要，组是用括号划分的正则表达式，可以通过编号来引用组。组号从0开始，有几对小括号就表示有几个组，并且组可以嵌套，组号为0的表示整个表达式，组号为1的表示第一个组，依此类推。
	例如：A(B)C(D)E正则式中有三组，组0是ABCDE，组1是B，组2是D；
	A((B)C)(D)E正则式中有四组：组0是ABCDE，组1是BC，组2是B；组3是C，组4是D。
	 
	int groupCount()：返回匹配其模式中组的数目，不包括第0组。
	String group()：返回前一次匹配操作（如find()）的第0组。
	String group(int group)：返回前一次匹配操作期间指定的组所匹配的子序列。如果该匹配成功，但指定组未能匹配字符序列的任何部分，则返回 null。
	int start(int group)：返回前一次匹配操作期间指定的组所匹配的子序列的初始索引。
	int end(int group)：返回前一次匹配操作期间指定的组所匹配的子序列的最后索引+1。*/
/**
 * @author fengyu
 * @date 2015年8月12日
 */
public class Groups
{
  static public final String POEM =
    "Twas brillig, and the slithy toves\n" +
    "Did gyre and gimble in the wabe.\n" +
    "All mimsy were the borogoves,\n" +
    "And the mome raths outgrabe.\n\n" +
    "Beware the Jabberwock, my son,\n" +
    "The jaws that bite, the claws that catch.\n" +
    "Beware the Jubjub bird, and shun\n" +
    "The frumious Bandersnatch.";
  public static void main(String[] args) {
		// 目的:捕获每行的最后3个单词,每行以$结束,正常情况下是将$与整个输入序列的末端相匹配,显式的告知正则表达式注意输入序列中的换行符,
	  //(?m)由序列开头的模式标记完成(?m),任意数目的非空格字符(\S+)以及随后的任意数目的空格字符(\s+)组成
		Matcher m=Pattern.compile("(?m)(\\S+)\\s+((\\S+)\\s+(\\S+))$").matcher(POEM);
		while(m.find())
		{
			// 返回匹配其模式中组的数目，不包括第0组。
			for(int j=0;j<=m.groupCount();j++)
				printnb("["+m.group(j)+"]");
			print();
		}
	}
} /* Output:
[the slithy toves][the][slithy toves][slithy][toves]
[in the wabe.][in][the wabe.][the][wabe.]
[were the borogoves,][were][the borogoves,][the][borogoves,]
[mome raths outgrabe.][mome][raths outgrabe.][raths][outgrabe.]
[Jabberwock, my son,][Jabberwock,][my son,][my][son,]
[claws that catch.][claws][that catch.][that][catch.]
[bird, and shun][bird,][and shun][and][shun]
[The frumious Bandersnatch.][The][frumious Bandersnatch.][frumious][Bandersnatch.]
*///:~
