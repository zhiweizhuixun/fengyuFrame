package cn.feng.thinkInJava.a8_2_字符串.strings.a13_06_07;
//: strings/SplitDemo.java
import java.util.regex.*;
import java.util.*;
import static net.mindview.util.Print.*;

public class SplitDemo {
  public static void main(String[] args) {
    String input =
      "This!!unusual use!!of exclamation!!points";
    print(Arrays.toString(//将字符串断开成字符串对象数组,断开边界有正则表达式确定
      Pattern.compile("!!").split(input)));
    // Only do the first three:
    print(Arrays.toString(//第二种形式split()方法限制输入分割成字符串的数量
      Pattern.compile("!!").split(input, 3)));
  }
} /* Output:
[This, unusual use, of exclamation, points]
[This, unusual use, of exclamation!!points]
*///:~
