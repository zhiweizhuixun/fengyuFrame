package cn.feng.thinkInJava.a8_2_字符串.strings.a13_05_02;

//: strings/Conversion.java
import java.math.*;
import java.util.*;

/**
 * Formatter转换:最常用的类型转换
 * 
 * @author fengyu
 * @date 2015年8月12日
 */
public class Conversion
{
	/**
	 * 被注释的表示转换无效,,如果执行这些转换,则会触发异常,
	 * 对于b:对于boolean基本类型或者Boolean对象,其结果对应是true后false,但是对于其他参数,只要该参数不是null,
	 * 那转换结果永远都是true,即使数字0也会转换结果也是true
	 * 
	 * @author fengyu
	 * @date 2015年8月12日
	 * @param args
	 */
	public static void main(String[] args)
	{
		Formatter f=new Formatter(System.out);
		char u='a';
		System.out.println("u = 'a'");
		f.format("s: %s\n",u);//s:String
		// f.format("d: %d\n", u);
		f.format("c: %c\n",u);//c:Unicode字符
		f.format("b: %b\n",u);
		// f.format("f: %f\n", u);
		// f.format("e: %e\n", u);
		// f.format("x: %x\n", u);
		f.format("h: %h\n",u);
		int v=121;
		System.out.println("v = 121");
		f.format("d: %d\n",v);//d:整数型(十进制)
		f.format("c: %c\n",v);
		f.format("b: %b\n",v);//b:Boolean值
		f.format("s: %s\n",v);
		// f.format("f: %f\n", v);
		// f.format("e: %e\n", v);
		f.format("x: %x\n",v);//x:整数(十六进制)
		f.format("h: %h\n",v);//h:散列码
		BigInteger w=new BigInteger("50000000000000");
		System.out.println("w = new BigInteger(\"50000000000000\")");
		f.format("d: %d\n",w);
		// f.format("c: %c\n", w);
		f.format("b: %b\n",w);
		f.format("s: %s\n",w);
		// f.format("f: %f\n", w);
		// f.format("e: %e\n", w);
		f.format("x: %x\n",w);
		f.format("h: %h\n",w);
		double x=179.543;
		System.out.println("x = 179.543");
		// f.format("d: %d\n", x);
		// f.format("c: %c\n", x);
		f.format("b: %b\n",x);
		f.format("s: %s\n",x);
		f.format("f: %f\n",x);//f:浮点数(十进制)
		f.format("e: %e\n",x);//e:浮点数(科学计数)
		// f.format("x: %x\n", x);
		f.format("h: %h\n",x);
		Conversion y=new Conversion();
		System.out.println("y = new Conversion()");
		// f.format("d: %d\n", y);
		// f.format("c: %c\n", y);
		f.format("b: %b\n",y);
		f.format("s: %s\n",y);
		// f.format("f: %f\n", y);
		// f.format("e: %e\n", y);
		// f.format("x: %x\n", y);
		f.format("h: %h\n",y);
		boolean z=false;
		System.out.println("z = false");
		// f.format("d: %d\n", z);
		// f.format("c: %c\n", z);
		f.format("b: %b\n",z);
		f.format("s: %s\n",z);
		// f.format("f: %f\n", z);
		// f.format("e: %e\n", z);
		// f.format("x: %x\n", z);
		f.format("h: %h\n",z);
	}
} /* Output: (Sample)
u = 'a'
s: a
c: a
b: true
h: 61
v = 121
d: 121
c: y
b: true
s: 121
x: 79
h: 79
w = new BigInteger("50000000000000")
d: 50000000000000
b: true
s: 50000000000000
x: 2d79883d2000
h: 8842a1a7
x = 179.543
b: true
s: 179.543
f: 179.543000
e: 1.795430e+02
h: 1ef462c
y = new Conversion()
b: true
s: Conversion@9cab16
h: 9cab16
z = false
b: false
s: false
h: 4d5
*///:~
