package cn.feng.thinkInJava.a8_2_字符串.strings.a13_05_00;
//: strings/SimpleFormat.java

/**
 * 格式化输出
 * 
 * @author fengyu
 * @date 2015年8月8日
 */
public class SimpleFormat
{
	public static void main(String[] args)
	{
		int x=5;
		double y=5.332542;
		// The old way:
		System.out.println("Row 1: ["+x+" "+y+"]");
		// The new way:Java SE5引进format方法
		System.out.format("Row 1: [%d %f]\n",x,y);// %d %f这些占位符称作 :格式化修饰符
		// or 两种dengjia  %d表示x是一个整数,%f表示y是以一个浮点数(float或者double)
		System.out.printf("Row 1: [%d %f]\n",x,y);
	}
} /*
	 * Output: Row 1: [5 5.332542] 
	 * 		   Row 1: [5 5.332542] 
	 *         Row 1: [5 5.332542]
	 */// :~
