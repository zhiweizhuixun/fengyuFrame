package cn.feng.thinkInJava.a8_2_字符串.strings.a13_06_01;

//: strings/Splitting.java
import java.util.*;

/**
 * 一个非常有用的正则表达式工具-split()方法,:其功能是"将字符串从正则表达式匹配的地方切开"
 * 
 * @author fengyu
 * @date 2015年8月12日
 */
public class Splitting
{
	public static String knights="Then, when you have found the shrubbery, you must "+"cut down the mightiest tree in the forest... "+"with... a herring!";
	
	public static void split(String regex)
			
	{//重载版本限制字符串的切割次数
		System.out.println(Arrays.toString(knights.split(regex)));
	}
	
	public static void main(String[] args)
	{
		split(" "); // Doesn't have to contain regex chars使用普通字符作为正则表达式,按空格来划分字符串
		split("\\W+"); // Non-word characters非单词字符
		split("n\\W+"); // 'n' followed by non-word characters字母n后面跟着一个或多个非单词字符
	}
} /* Output:
[Then,, when, you, have, found, the, shrubbery,, you, must, cut, down, the, mightiest, tree, in, the, forest..., with..., a, herring!]
[Then, when, you, have, found, the, shrubbery, you, must, cut, down, the, mightiest, tree, in, the, forest, with, a, herring]
[The, whe, you have found the shrubbery, you must cut dow, the mightiest tree i, the forest... with... a herring!]
*///:~
