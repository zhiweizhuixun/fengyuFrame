package cn.feng.thinkInJava.a8_2_字符串.strings.a13_06_09;

//: strings/Resetting.java
import java.util.regex.*;

public class Resetting
{
	public static void main(String[] args) throws Exception
	{
		Matcher m=Pattern.compile("[frb][aiu][gx]").matcher("fix the rug with bags");
		while(m.find())
			System.out.print(m.group()+" ");
		System.out.println();
		//可将现有的Mather对象应用于新的字符序列
		m.reset("fix the rig with rags");
		while(m.find())
			 System.out.print(m.group() + " ");
  }
} /* Output:
fix rug bag
fix rig rag
*///:~
