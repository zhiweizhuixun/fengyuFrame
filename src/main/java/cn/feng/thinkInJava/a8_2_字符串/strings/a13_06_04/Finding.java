package cn.feng.thinkInJava.a8_2_字符串.strings.a13_06_04;

//: strings/Finding.java
import java.util.regex.*;
import static net.mindview.util.Print.*;

/**
 * Matcher.find()可以用来在CharSequence中查找多个匹配,CharSequence从CharBuffer,String,
 * StringBuffer,StringBuilder类中抽出来的字符串序列的一般定义
 * 
 * @author fengyu
 * @date 2015年8月12日
 */
public class Finding
{
	public static void main(String[] args)
	{
		Matcher m=Pattern.compile("\\w+").matcher("Evening is full of the linnet's wings");
		while(m.find())
			printnb(m.group()+" ");
		print("-------------------------");
		int i=0;//表示字符串中字符的位置,并以其为起点
		while(m.find(i))
		{
			printnb(m.group()+" ----");
			i++;
		}
	}
} /*
	 * Output: Evening is full of the linnet s wings Evening vening ening ning
	 * ing ng g is is s full full ull ll l of of f the the he e linnet linnet
	 * innet nnet net et t s s wings wings ings ngs gs s
	 */// :~
