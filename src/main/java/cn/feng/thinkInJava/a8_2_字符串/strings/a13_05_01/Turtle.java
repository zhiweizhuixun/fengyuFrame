package cn.feng.thinkInJava.a8_2_字符串.strings.a13_05_01;

//: strings/Turtle.java
import java.io.*;
import java.util.*;

public class Turtle
{
	private String name;
	private Formatter f;
	
	/**
	 * java.util.Formatter类处理所有新的格式化功能 可以将Formatter看做一个翻译器,将格式化字符串与数据翻译成需要的结果
	 * 
	 * @author fengyu
	 * @date 2015年8月12日
	 * @param name
	 * @param f
	 */
	public Turtle(String name,Formatter f)
	{
		this.name=name;
		this.f=f;
	}
	
	public void move(int x,int y)
	{			//%s表示插入的参数是String类型
		f.format("%s The Turtle is at (%d,%d)\n",name,x,y);
	}
	
	public static void main(String[] args)
	{
		PrintStream outAlias=System.out;
										//创建一个Formatter对象需要向其构造器传递一些信息,告知它最终的结果需要向哪里输出
		Turtle tommy=new Turtle("Tommy",new Formatter(System.out));
							//最常用的还是PrintStream(),OutputStream和File
		Turtle terry=new Turtle("Terry",new Formatter(outAlias));
		tommy.move(0,0);
		terry.move(4,8);
		tommy.move(3,4);
		terry.move(2,5);
		tommy.move(3,3);
		terry.move(3,3);
	}
} /*
	 *Tommy The Turtle is at (0,0)
	 *Terry The Turtle is at (4,8)
	 *Tommy The Turtle is at (3,4)
	 *Terry The Turtle is at (2,5)
	 *Tommy The Turtle is at (3,3)
	 *Terry The Turtle is at (3,3)

	 */// :~
