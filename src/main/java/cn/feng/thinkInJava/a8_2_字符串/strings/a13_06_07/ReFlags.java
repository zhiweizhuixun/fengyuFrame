package cn.feng.thinkInJava.a8_2_字符串.strings.a13_06_07;
//: strings/ReFlags.java
import java.util.regex.*;

/**Pattern标记:Pattern类的compile()方法还有另外一个版本,接受一个标记参数,以调整匹配的行为
 * @author fengyu
 * @date  2015年8月12日
 */
public class ReFlags {
  public static void main(String[] args) {
    Pattern p =  Pattern.compile("^java",
      Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
    Matcher m = p.matcher(
      "java has regex\nJava has regex\n" +
      "JAVA has pretty good regular expressions\n" +
      "Regular expressions are in Java");
    while(m.find())
      System.out.println(m.group());
  }
} /* Output:
java
Java
JAVA
*///:~
