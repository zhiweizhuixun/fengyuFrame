package cn.feng.thinkInJava.a8_2_字符串.strings.a13_01_00;

//: strings/Immutable.java
import static net.mindview.util.Print.*;

//不可变的String
public class Immutable
{
	// upcase运行时局部引用s才存在,一旦运行结束s引用就消失了
	public static String upcase(String s)// 每当把String对象作为方法的参数时候,都会复制一份引用
											// ,而该引用所指的对象其实一直待在单一的物理位置上,从未动过
	{
		return s.toUpperCase();
	}
	
	/**
	 * 对于一个方法而言,参数是为该方法提供信息的,而不是让该方法改变自己的
	 * 
	 * @author fengyu
	 * @date 2015年8月12日
	 * @param args
	 */
	public static void main(String[] args)
	{
		String q="howdy";
		print(q); // howdy
		String qq=upcase(q);// upcase返回的引用已经指向一个新的对象,而q则还留在原地
		print(qq); // HOWDY
		print(q); // howdy
	}
} /*
	 * Output: howdy HOWDY howdy
	 */// :~
