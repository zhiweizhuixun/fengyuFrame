package cn.feng.thinkInJava.a8_2_字符串.strings.a13_02_00;

//: strings/Concatenation.java
/**
 * 重载"+"与StringBuilder
 * 
 * @author fengyu
 * @date 2015年8月8日
 */
// 连结 ,可以String对象任意多的别名,因为String对象具有只读特性,所以指向他的任何引用多不能改变它的值,因此也不会对其他引用有什么影响
public class Concatenation
{
	public static void main(String[] args)
	{
		String mango="mango";
		String s="abc"+mango+"def"+47;// 这种方式产生一大堆需要垃圾回收的中间对象
		System.out.println(s);
		// java仅有的两个重载过的操作符:用于String的"+"与"+="
	}
} /*
	 * Output: abcmangodef47
	 */// :~
