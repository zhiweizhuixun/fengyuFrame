package cn.feng.thinkInJava.a5_1_初始化;

//非静态实例初始化
//: initialization/Mugs.java
// Java "Instance Initialization."
import static net.mindview.util.Print.*;

class Mug {
	Mug(int marker) {
		print("Mug(" + marker + ")");
	}

	void f(int marker) {
		print("f(" + marker + ")");
	}
}

public class Mugs {
	int c=123;
	static int i;
	static {
		i = 2;
		b = 3;
	}
	int d;
	static int b = 2;
	Mug mug1;
	{
		mug1 = new Mug(1);
		mug2 = new Mug(2);
		print("mug1 & mug2 initialized");
	}
	Mug mug2;

	Mugs() {
		print("Mugs()");
	}

	Mugs(int i) {
		print("Mugs(int)");
	}

	public static void main(String[] args) {
		print("Inside main()");
		new Mugs();
		print("new Mugs() completed");
		new Mugs(1);
		print("new Mugs(1) completed");
	}
} /*
 * Output: Inside main() Mug(1) Mug(2) mug1 & mug2 initialized Mugs() new Mugs()
 * completed Mug(1) Mug(2) mug1 & mug2 initialized Mugs(int) new Mugs(1)
 * completed
 */// :~
