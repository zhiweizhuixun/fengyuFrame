package cn.feng.thinkInJava.a1_5_初始化和清理.initialization.a5_04_00;
//: initialization/Leaf.java

// Simple use of the "this" keyword.
public class Leaf
{
	int i=0;
	
	Leaf increment()
	{
		i++;
		return this;
	}
	
	void print()
	{
		System.out.println("i = "+i);
	}
	
	public static void main(String[] args)
	{
		Leaf x=new Leaf();
		x.increment().increment().increment().print();
	}
} /* Output:
i = 3
*///:~
