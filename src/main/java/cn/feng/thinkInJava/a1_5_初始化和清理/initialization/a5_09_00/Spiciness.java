package cn.feng.thinkInJava.a1_5_初始化和清理.initialization.a5_09_00;
//: initialization/Spiciness.java

/**枚举类型
 * @author fengyu
 * @date  2015年8月9日
 */
public enum Spiciness {
  NOT, MILD, MEDIUM, HOT, FLAMING
} ///:~
