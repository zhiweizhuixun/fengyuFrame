package cn.feng.thinkInJava.a1_5_初始化和清理.initialization.a5_06_01;
//: initialization/MethodInit2.java
public class MethodInit2 {
  int i = f();
  int j = g(i);
  int f() { return 11; }
  int g(int n) { return n * 10; }
} ///:~
