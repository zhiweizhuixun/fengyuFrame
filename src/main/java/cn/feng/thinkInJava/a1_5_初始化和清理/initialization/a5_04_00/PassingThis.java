package cn.feng.thinkInJava.a1_5_初始化和清理.initialization.a5_04_00;
//: initialization/PassingThis.java

class Person
{
	public void eat(Apple apple)
	{
		Apple peeled=apple.getPeeled();
		System.out.println("Yummy");
	}
}

class Peeler
{
	static Apple peel(Apple apple)
	{
		// ... remove peel
		return apple; // Peeled
	}
}

class Apple
{
	Apple getPeeled()
	{
		return Peeler.peel(this);
	}
}

public class PassingThis
{
	public static void main(String[] args)
	{
		new Person().eat(new Apple());
	}
} /* Output:
Yummy
*///:~
