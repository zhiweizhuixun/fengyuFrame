package cn.feng.thinkInJava.a1_5_初始化和清理.initialization.a5_07_03;
//: initialization/Spoon.java
/**显示静态初始化
 * @author fengyu
 * @date  2015年8月9日
 */
public class Spoon {
  static int i;
  static {
    i = 47;
  }
} ///:~
