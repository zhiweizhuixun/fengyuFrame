package cn.feng.thinkInJava.a1_5_初始化和清理.initialization.a5_08_00;

//: initialization/ArrayInit.java
// Array initialization.
import java.util.*;

public class ArrayInit
{
	public static void main(String[] args)
	{// Autoboxing
		Integer[] a= { new Integer(1), new Integer(2), 3, };
		// Autoboxing
		Integer[] b=new Integer[] { new Integer(1), new Integer(2), 3, };
		System.out.println(Arrays.toString(a));
		System.out.println(Arrays.toString(b));
	}
} /* Output:
[1, 2, 3]
[1, 2, 3]
*///:~
