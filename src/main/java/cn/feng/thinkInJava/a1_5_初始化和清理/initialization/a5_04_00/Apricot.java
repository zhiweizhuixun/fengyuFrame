package cn.feng.thinkInJava.a1_5_初始化和清理.initialization.a5_04_00;
//: initialization/Apricot.java
public class Apricot {
  void pick() { /* ... */ }
  void pit() { pick(); /* ... */ }
} ///:~
