package cn.feng.thinkInJava.a1_5_初始化和清理.initialization.a5_03_00;
//: initialization/DefaultConstructor.java

class Bird {}

/**默认构造器
 * @author fengyu
 * @date  2015年8月9日
 */
public class DefaultConstructor {
  public static void main(String[] args) {
    Bird b = new Bird(); // Default!
  }
} ///:~
