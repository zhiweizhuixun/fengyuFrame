package cn.feng.thinkInJava.a1_5_初始化和清理.initialization.a5_04_01;

//: initialization/Flower.java
// Calling constructors with "this"
import static net.mindview.util.Print.*;

/**
 * 在构造器中调用构造器
 * 
 * @author fengyu
 * @date 2015年8月9日
 */
public class Flower
{
	int petalCount=0;
	String s="initial value";
	
	Flower(int petals)//petals花瓣
	{
		petalCount=petals;
		print("Constructor w/ int arg only, petalCount= "+petalCount);
	}
	
	Flower(String ss)
	{
		print("Constructor w/ String arg only, s = "+ss);
		s=ss;
	}
	
	Flower(String s,int petals)
	{
		this(petals);
		// ! this(s); // Can't call two!
		this.s=s; // Another use of "this"
		print("String & int args");
	}
	
	Flower()
	{
		this("hi",47);
		print("default constructor (no args)");
	}
	
	void printPetalCount()
	{
		// ! this(11); // Not inside non-constructor!
		print("petalCount = "+petalCount+" s = "+s);
	}
	
	public static void main(String[] args)
	{
		Flower x=new Flower();
		x.printPetalCount();
	}
} /* Output:
Constructor w/ int arg only, petalCount= 47
String & int args
default constructor (no args)
petalCount = 47 s = hi
*///:~
