package cn.feng.thinkInJava.a1_5_初始化和清理.initialization.a5_08_00;

//: initialization/ArrayNew.java
// Creating arrays with new.
import java.util.*;
import static net.mindview.util.Print.*;

public class ArrayNew
{
	public static void main(String[] args)
	{
		int[] a;
		Random rand=new Random(47);
		a=new int[rand.nextInt(20)];
		print("length of a = "+a.length);
		print(Arrays.toString(a));
	}
} /* Output:
length of a = 18
[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
*///:~
