package cn.feng.thinkInJava.a8_1_IO.io.a18_06_02;
//: io/MemoryInput.java
import java.io.IOException;
import java.io.StringReader;

import cn.feng.thinkInJava.a8_1_IO.io.a18_06_01.BufferedInputFile;

/**从内存输入
 * @author fengyu
 * @date  2015年8月9日
 */
public class MemoryInput {
  public static void main(String[] args)
  throws IOException {
    StringReader in = new StringReader(
      BufferedInputFile.read("MemoryInput.java"));
    int c;
    while((c = in.read()) != -1)
      System.out.print((char)c);
  }
} /* (Execute to see output) *///:~
