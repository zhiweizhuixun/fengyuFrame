package cn.feng.thinkInJava.a8_1_IO.io.a18_06_03;
//: io/FormattedMemoryInput.java
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;

import cn.feng.thinkInJava.a8_1_IO.io.a18_06_01.BufferedInputFile;

/**格式化内存输入
 * @author fengyu
 * @date  2015年8月9日
 */
public class FormattedMemoryInput {
  public static void main(String[] args)
  throws IOException {
    try {
      DataInputStream in = new DataInputStream(
        new ByteArrayInputStream(
         BufferedInputFile.read(
          "FormattedMemoryInput.java").getBytes()));
      while(true)
        System.out.print((char)in.readByte());
    } catch(EOFException e) {
      System.err.println("End of stream");
    }
  }
} /* (Execute to see output) *///:~
