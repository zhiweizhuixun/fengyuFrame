package cn.feng.thinkInJava.a8_1_IO.io.a18_08_02;
//: io/ChangeSystemOut.java
// Turn System.out into a PrintWriter.
import java.io.*;

/**将System.out转换为PrintWrite
 * @author fengyu
 * @date  2015年8月9日
 */
public class ChangeSystemOut {
  public static void main(String[] args) {
    PrintWriter out = new PrintWriter(System.out, true);
    out.println("Hello, world");
  }
} /* Output:
Hello, world
*///:~
