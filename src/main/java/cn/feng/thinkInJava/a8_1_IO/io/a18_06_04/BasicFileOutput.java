package cn.feng.thinkInJava.a8_1_IO.io.a18_06_04;
//: io/BasicFileOutput.java
import java.io.*;

import cn.feng.thinkInJava.a8_1_IO.io.a18_06_01.BufferedInputFile;

/**基本的文件输出
 * @author fengyu
 * @date  2015年8月9日
 */
public class BasicFileOutput {
  static String file = "BasicFileOutput.out";
  public static void main(String[] args)
  throws IOException {
    BufferedReader in = new BufferedReader(
      new StringReader(
        BufferedInputFile.read("BasicFileOutput.java")));
    PrintWriter out = new PrintWriter(
      new BufferedWriter(new FileWriter(file)));
    int lineCount = 1;
    String s;
    while((s = in.readLine()) != null )
      out.println(lineCount++ + ": " + s);
    out.close();
    // Show the stored file:
    System.out.println(BufferedInputFile.read(file));
  }
} /* (Execute to see output) *///:~
