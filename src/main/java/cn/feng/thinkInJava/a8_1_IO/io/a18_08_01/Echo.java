package cn.feng.thinkInJava.a8_1_IO.io.a18_08_01;
//: io/Echo.java
// How to read from standard input.
// {RunByHand}
import java.io.*;

/**从标准输入中读取
 * @author fengyu
 * @date  2015年8月9日
 */
public class Echo {
  public static void main(String[] args)
  throws IOException {
    BufferedReader stdin = new BufferedReader(
      new InputStreamReader(System.in));
    String s;
    while((s = stdin.readLine()) != null && s.length()!= 0)
      System.out.println(s);
    // An empty line or Ctrl-Z terminates the program
  }
} ///:~
