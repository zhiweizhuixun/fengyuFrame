//: annotations/ExtractInterface.java
// APT-based annotation processing.
package cn.feng.thinkInJava.b2_1_注解.annotations.a20_03_00;
import java.lang.annotation.*;

/**使用apt处理注解
 * @author fengyu
 * @date  2015年8月9日
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface ExtractInterface {
  public String value();
} ///:~
