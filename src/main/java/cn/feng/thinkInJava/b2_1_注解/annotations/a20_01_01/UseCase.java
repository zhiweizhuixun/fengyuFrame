package cn.feng.thinkInJava.b2_1_注解.annotations.a20_01_01;
//: annotations/UseCase.java
import java.lang.annotation.*;

/**id和description类似方法定义.由于编译器会对id进行类型检查
 * @author fengyu
 * @date  2015年8月9日
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface UseCase {
  public int id();
  //在注解某个方法时没有给出的description的值,则该注解处理器就会使用此元素的默认值
  public String description() default "no description";
} ///:~

//注解元素可用的类型
//1所有的基本类型
//String
//Class
//enum
//Annotation
//以上类型数组
//使用了其他类型编译器就会报错.注意也不允许使用任何包装类型,不过由于自动打包的存在,这也算不上什么限制
