//: annotations/Testable.java
package cn.feng.thinkInJava.b2_1_注解.annotations.a20_01_01;

import net.mindview.atunit.*;

/**
 * 基本语法
 * 
 * @author fengyu
 * @date 2015年8月9日
 */
public class Testable
{
	public void execute()
	{
		System.out.println("Executing..");
	}
	
	/**注解本身并不做任何事情,但是编译器要确保在构造路径上必须有@Test注解定义,被注解的方法与其他方法没有区别
			注解@Test可以与任何修饰符共同作用于方法,例如publi,static或void,从语法的角度,注解的使用方式机会与修饰符
			的一模一样
		*/
	@Test
	void testExecute()
	{
		execute();
	}
} /// :~
