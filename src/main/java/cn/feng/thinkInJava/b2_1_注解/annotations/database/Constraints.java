//: annotations/database/Constraints.java
package cn.feng.thinkInJava.b2_1_注解.annotations.database;
import java.lang.annotation.*;

/**修饰javaBean域准备的注解
 * @author fengyu
 * @date  2015年8月9日
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Constraints {
  boolean primaryKey() default false;
  boolean allowNull() default true;
  boolean unique() default false;
} ///:~
