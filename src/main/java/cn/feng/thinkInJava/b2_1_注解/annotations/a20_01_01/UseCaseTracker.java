package cn.feng.thinkInJava.b2_1_注解.annotations.a20_01_01;

//: annotations/UseCaseTracker.java
import java.lang.reflect.*;
import java.util.*;

/**
 * 编写注解处理器
 * 
 * @author fengyu
 * @date 2015年8月9日
 */
public class UseCaseTracker
{	//getDeclaredMethods和getAnnotation都属于AnnotatedElement接口
	public static void trackUseCases(List<Integer> useCases,Class<?> cl)
	{	//getDeclaredMethods
		for(Method m:cl.getDeclaredMethods())
		{//getAnnotation方法返回指定类型的注解对象,在这里就是UseCase,如果被注解的方法上没有该类型的注解,则返回null值
			UseCase uc=m.getAnnotation(UseCase.class);
			if(uc!=null)
			{	//调用id()和description()方法从返回的UserCase对象中提取元素的值
				System.out.println("Found Use Case:"+uc.id()+" "+uc.description());
				useCases.remove(new Integer(uc.id()));
			}
		}
		for(int i:useCases)
		{
			System.out.println("Warning: Missing use case-"+i);
		}
	}
	
	public static void main(String[] args)
	{
		List<Integer> useCases=new ArrayList<Integer>();
		Collections.addAll(useCases,47,48,49,50);
		
		//读取PasswordUtils并用反射机制查找@UseCase
		trackUseCases(useCases,PasswordUtils.class);
	}
} /* Output:
Found Use Case:47 Passwords must contain at least one numeric
Found Use Case:48 no description
Found Use Case:49 New passwords can't equal previously used ones
Warning: Missing use case-50
*///:~
