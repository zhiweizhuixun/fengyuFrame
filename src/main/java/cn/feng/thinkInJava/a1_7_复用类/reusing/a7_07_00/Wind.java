package cn.feng.thinkInJava.a1_7_复用类.reusing.a7_07_00;
//: reusing/Wind.java

// Inheritance & upcasting.
/**
 * 向上转型
 * 
 * @author fengyu
 * @date 2015年8月9日
 */
class Instrument
{
	public void play()
	{
	}
	
	static void tune(Instrument i)
	{
		// ...
		i.play();
	}
}

// Wind objects are instruments
// because they have the same interface:
public class Wind extends Instrument
{
	public static void main(String[] args)
	{
		Wind flute=new Wind();
		Instrument.tune(flute); // Upcasting
	}
} /// :~
