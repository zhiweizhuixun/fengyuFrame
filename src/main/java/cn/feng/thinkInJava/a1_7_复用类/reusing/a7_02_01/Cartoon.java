package cn.feng.thinkInJava.a1_7_复用类.reusing.a7_02_01;

//: reusing/Cartoon.java
// Constructor calls during inheritance.
import static net.mindview.util.Print.*;

/**
 * 初始化基类
 * 
 * @author fengyu
 * @date 2015年8月9日
 */
class Art
{
	Art()
	{
		print("Art constructor");
	}
}

class Drawing extends Art
{
	Drawing()
	{
		print("Drawing constructor");
	}
}

public class Cartoon extends Drawing
{
	public Cartoon()
	{
		print("Cartoon constructor");
	}
	
	public static void main(String[] args)
	{
		Cartoon x=new Cartoon();
	}
} /* Output:
Art constructor
Drawing constructor
Cartoon constructor
*///:~
