package cn.feng.thinkInJava.a1_7_复用类.reusing.a7_01_00;

public class SpaceShip extends SpaceShipControls
{
	private String name;
	
	public SpaceShip(String name)
	{
		this.name=name;
	}
	
	public String toString()
	{
		return name;
	}
	
	public static void main(String[] args)
	{
		SpaceShip protector=new SpaceShip("NSEA Protector");
		protector.forward(100);
	}
} /// :~
