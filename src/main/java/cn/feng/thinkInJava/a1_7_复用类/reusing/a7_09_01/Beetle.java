package cn.feng.thinkInJava.a1_7_复用类.reusing.a7_09_01;

//: reusing/Beetle.java
// The full process of initialization.
import static net.mindview.util.Print.*;

/**
 * 继承和初始化
 * 
 * @author fengyu
 * @date 2015年8月9日
 */
class Insect
{
	private int i=9;
	protected int j;
	
	Insect()
	{
		print("i = "+i+", j = "+j);
		j=39;
	}
	private static int x1=printInit("static Insect.x1 initialized");
	
	static int printInit(String s)
	{
		print(s);
		return 47;
	}
}

public class Beetle extends Insect
{
	private int k=printInit("Beetle.k initialized");
	
	public Beetle()
	{
		print("k = "+k);
		print("j = "+j);
	}
	private static int x2=printInit("static Beetle.x2 initialized");
	
	public static void main(String[] args)
	{
		print("Beetle constructor");
		Beetle b=new Beetle();
	}
} /* Output:
static Insect.x1 initialized
static Beetle.x2 initialized
Beetle constructor
i = 9, j = 0
Beetle.k initialized
k = 47
j = 39
*///:~
