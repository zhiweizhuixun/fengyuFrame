package cn.feng.thinkInJava.a1_7_复用类.reusing.a7_08_00;
//: reusing/FinalArguments.java

// Using "final" with method arguments.
/**
 * final 参数
 * 
 * @author fengyu
 * @date 2015年8月9日
 */
class Gizmo
{
	public void spin()
	{
	}
}

public class FinalArguments
{
	void with(final Gizmo g)
	{
		// ! g = new Gizmo(); // Illegal -- g is final
	}
	
	void without(Gizmo g)
	{
		g=new Gizmo(); // OK -- g not final
		g.spin();
	}
	
	// void f(final int i) { i++; } // Can't change
	// You can only read from a final primitive:
	int g(final int i)
	{
		return i+1;
	}
	
	public static void main(String[] args)
	{
		FinalArguments bf=new FinalArguments();
		bf.without(null);
		bf.with(null);
	}
} /// :~
