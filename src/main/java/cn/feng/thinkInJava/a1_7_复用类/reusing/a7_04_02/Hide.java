package cn.feng.thinkInJava.a1_7_复用类.reusing.a7_04_02;

//: reusing/Hide.java
// Overloading a base-class method name in a derived
// class does not hide the base-class versions.
import static net.mindview.util.Print.*;

/**
 * 名称屏蔽
 * 
 * @author fengyu
 * @date 2015年8月9日
 */
class Homer
{
	char doh(char c)
	{
		print("doh(char)");
		return 'd';
	}
	
	float doh(float f)
	{
		print("doh(float)");
		return 1.0f;
	}
}

class Milhouse
{
}

class Bart extends Homer
{
	void doh(Milhouse m)
	{
		print("doh(Milhouse)");
	}
}

public class Hide
{
	public static void main(String[] args)
	{
		Bart b=new Bart();
		b.doh(1);
		b.doh('x');
		b.doh(1.0f);
		b.doh(new Milhouse());
	}
} /* Output:
doh(float)
doh(char)
doh(float)
doh(Milhouse)
*///:~
