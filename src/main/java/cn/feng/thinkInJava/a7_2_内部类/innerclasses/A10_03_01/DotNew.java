package cn.feng.thinkInJava.a7_2_内部类.innerclasses.A10_03_01;
//: innerclasses/DotNew.java
// Creating an inner class directly using the .new syntax.
/**使用.new
 * @author fengyu
 * @date  2015年8月8日
 */
public class DotNew {
  public class Inner {}
  public static void main(String[] args) {
    DotNew dn = new DotNew();
    //使用new表达式时候要提供外部类对象的引用;语法:.new
    DotNew.Inner dni = dn.new Inner();
  }
} ///:~
