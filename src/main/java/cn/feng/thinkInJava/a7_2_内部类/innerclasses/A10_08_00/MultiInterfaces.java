//: innerclasses/MultiInterfaces.java
// Two ways that a class can implement multiple interfaces.
package cn.feng.thinkInJava.a7_2_内部类.innerclasses.A10_08_00;

interface A {}
interface B {}

class X implements A, B {}

class Y implements A {
  B makeB() {
    // Anonymous inner class:
    return new B() {};
  }
}
//使用单一的类,或者使用内部类
public class MultiInterfaces {
  static void takesA(A a) {}
  static void takesB(B b) {}
  public static void main(String[] args) {
    X x = new X();
    Y y = new Y();
    takesA(x);
    takesA(y);
    takesB(x);
    takesB(y.makeB());
  }
} ///:~
