package cn.feng.thinkInJava.a7_2_内部类.innerclasses.a10_03_00;
//: innerclasses/DotThis.java
// Qualifying access to the outer-class object.

/**使用.this
 * @author fengyu
 * @date  2015年8月8日
 */
public class DotThis {
  void f() { System.out.println("DotThis.f()"); }
  public class Inner {
    public DotThis outer() {
    	//生成一个外部类对象的引用:外部类的名字.this;在编译期知晓,没有任何运行开销
      return DotThis.this;
      // A plain "this" would be Inner's "this"
    }
  }
  public Inner inner() { return new Inner(); }
  public static void main(String[] args) {
    DotThis dt = new DotThis();
    DotThis.Inner dti = dt.inner();
    dti.outer().f();
  }
} /* Output:
DotThis.f()
*///:~
