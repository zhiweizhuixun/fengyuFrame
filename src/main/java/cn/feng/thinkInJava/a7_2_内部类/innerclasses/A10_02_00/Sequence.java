package cn.feng.thinkInJava.a7_2_内部类.innerclasses.A10_02_00;
//: innerclasses/Sequence.java
// Holds a sequence of Objects.

/**链接到外部类
 * @author fengyu
 * @date  2015年8月8日
 */
interface Selector {
  boolean end();
  Object current();
  void next();
}	

/**Sequence固定大小的Object数组,以类打得形式包装起来
 * @author fengyu
 * @date  2015年8月8日
 */
public class Sequence {
  private Object[] items;
  private int next = 0;
  public Sequence(int size) { items = new Object[size]; }
  public void add(Object x) {
    if(next < items.length)
      items[next++] = x;
  }
  /**SequenceSelector提供 Selector功能的private类
 * @author fengyu
 * @date  2015年8月8日
 */
private class SequenceSelector implements Selector {
    private int i = 0;
    public boolean end() { return i == items.length; }
    public Object current() { return items[i]; }
    public void next() { if(i < items.length) i++; }
  }
  public Selector selector() {
    return new SequenceSelector();
  }	
  public static void main(String[] args) {
	  Sequence a10_02_00_Sequence = new Sequence(10);
    for(int i = 0; i < 10; i++)
      a10_02_00_Sequence.add(Integer.toString(i));
    Selector selector = a10_02_00_Sequence.selector();
    while(!selector.end()) {
      System.out.print(selector.current() + " ");
      selector.next();
    }
  }
} /* Output:
0 1 2 3 4 5 6 7 8 9
*///:~
