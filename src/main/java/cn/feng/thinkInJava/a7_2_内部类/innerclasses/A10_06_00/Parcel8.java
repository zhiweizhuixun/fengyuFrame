package cn.feng.thinkInJava.a7_2_内部类.innerclasses.A10_06_00;

public class Parcel8 {
	public Wrapping wrapping(int x) {
		// Base constructor call://传递参数的匿名内部类
		return new Wrapping(x) { // Pass constructor argument.
			public int value() {
				return super.value() * 47;
			}
		}; // Semicolon required
	}

	public static void main(String[] args) {
		Parcel8 p = new Parcel8();
		Wrapping w = p.wrapping(10);
	}
} // /:~
