package cn.feng.thinkInJava.a7_2_内部类.innerclasses.A10_07_01;

//: innerclasses/ClassInInterface.java
// {main: ClassInInterface$Test}

/**
 * 接口内部的类                                                                                              
 */
public interface ClassInInterface {                    
	void howdy();

	/**嵌套类可以作为接口的一部分,
	 */
	class Test implements ClassInInterface {
		public void howdy() {
			System.out.println("Howdy!");
		}

		public static void main(String[] args) {
			new Test().howdy();
		}
	}
} /*
 * Output: Howdy!
 */// :~
