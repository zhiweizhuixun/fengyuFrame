package cn.feng.thinkInJava.a7_2_内部类.innerclasses.A10_06_00;

import cn.feng.thinkInJava.a7_2_内部类.innerclasses.A10_04_00.Contents;
//: innerclasses/Parcel7.java
// Returning an instance of an anonymous inner class.

public class Parcel7 {
	public Contents contents() {
		return
		/** 匿名内部类 */
		new Contents() { // Insert a class definition
			private int i = 11;

			public int value() {
				return i;
			}
		}; // Semicolon required in this case
	}

	public static void main(String[] args) {
		Parcel7 p = new Parcel7();
		Contents c = p.contents();
	}
} // /:~
