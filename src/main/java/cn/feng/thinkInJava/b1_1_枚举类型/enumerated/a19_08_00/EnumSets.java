//: enumerated/EnumSets.java
// Operations on EnumSets
package cn.feng.thinkInJava.b1_1_枚举类型.enumerated.a19_08_00;
import static net.mindview.util.Print.print;
import static cn.feng.thinkInJava.b1_1_枚举类型.enumerated.a19_08_00.AlarmPoints.*;
import java.util.EnumSet;

/**使用EnumSet代替标志:EnumSet是为了通过enum创建一种替代品,以替代传统的基于int的"位标志"
 * 这种标志可以用来表示某种"开/关"信息,不过这种标志,最终操作的只是一些bit,而不是zhexiebit想要表达的概念,因此很容易写出令人
 * 难以理解的代码
 * 
 * EnumSet的设计充分的考虑到了速度因素,因为它必须与非常搞笑的bit标志相竞争(其操作与HashSet相比非常的快)
 * 使用EnumSet的优点是,它在说明一个二进制位是否存在存在时,具有更好的表达能力,并且无需担心性能
 * @author fengyu
 * @date  2015年8月9日
 */
public class EnumSets {
  public static void main(String[] args) {
    EnumSet<AlarmPoints> points =
      EnumSet.noneOf(AlarmPoints.class); // Empty set
    points.add(BATHROOM);
    print(points);
    points.addAll(EnumSet.of(STAIR1, STAIR2, KITCHEN));
    print(points);
    points = EnumSet.allOf(AlarmPoints.class);
    points.removeAll(EnumSet.of(STAIR1, STAIR2, KITCHEN));
    print(points);
    points.removeAll(EnumSet.range(OFFICE1, OFFICE4));
    print(points);
    points = EnumSet.complementOf(points);
    print(points);
  }
} /* Output:
[BATHROOM]
[STAIR1, STAIR2, BATHROOM, KITCHEN]
[LOBBY, OFFICE1, OFFICE2, OFFICE3, OFFICE4, BATHROOM, UTILITY]
[LOBBY, BATHROOM, UTILITY]
[STAIR1, STAIR2, OFFICE1, OFFICE2, OFFICE3, OFFICE4, KITCHEN]
*///:~
