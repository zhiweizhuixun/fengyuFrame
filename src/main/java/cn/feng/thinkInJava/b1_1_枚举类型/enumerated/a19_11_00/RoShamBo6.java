//: enumerated/RoShamBo6.java
// Enums using "tables" instead of multiple dispatch.
package cn.feng.thinkInJava.b1_1_枚举类型.enumerated.a19_11_00;
import static cn.feng.thinkInJava.b1_1_枚举类型.enumerated.a19_11_00.Outcome.*;

/**使用二维数组
 * @author fengyu
 * @date  2015年8月9日
 */
enum RoShamBo6 implements Competitor<RoShamBo6> {
  PAPER, SCISSORS, ROCK;
  private static Outcome[][] table = {
    { DRAW, LOSE, WIN }, // PAPER
    { WIN, DRAW, LOSE }, // SCISSORS
    { LOSE, WIN, DRAW }, // ROCK
  };
  public Outcome compete(RoShamBo6 other) {
    return table[this.ordinal()][other.ordinal()];
  }
  public static void main(String[] args) {
    RoShamBo.play(RoShamBo6.class, 20);
  }
} ///:~
