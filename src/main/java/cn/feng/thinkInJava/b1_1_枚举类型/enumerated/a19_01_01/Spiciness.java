//: enumerated/Spiciness.java
package cn.feng.thinkInJava.b1_1_枚举类型.enumerated.a19_01_01;

public enum Spiciness
{
	/**
	 * not
	 */
	NOT,MILD,MEDIUM,HOT,FLAMING
} /// :~
