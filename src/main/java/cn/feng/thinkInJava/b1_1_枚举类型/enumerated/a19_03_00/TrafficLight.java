package cn.feng.thinkInJava.b1_1_枚举类型.enumerated.a19_03_00;

//: enumerated/TrafficLight.java
// Enums in switch statements.
import static net.mindview.util.Print.*;

// Define an enum type:
enum Signal
{
	GREEN,YELLOW,RED,
}

/**
 * switch中语句中的enum
 * 
 * @author fengyu
 * @date 2015年8月9日
 */
public class TrafficLight
{
	Signal color=Signal.RED;
	
	public void change()
	{
		switch(color)
		{
		// Note that you don't have to say Signal.RED
		// in the case statement:
			case RED:
				color=Signal.GREEN;
				break;
			case GREEN:
				color=Signal.YELLOW;
				break;
			case YELLOW:
				color=Signal.RED;
				break;
		}
	}
	
	public String toString()
	{
		return "The traffic light is "+color;
	}
	
	public static void main(String[] args)
	{
		TrafficLight t=new TrafficLight();
		for(int i=0;i<7;i++)
		{
			print(t);
			t.change();
		}
  }
} /* Output:
The traffic light is RED
The traffic light is GREEN
The traffic light is YELLOW
The traffic light is RED
The traffic light is GREEN
The traffic light is YELLOW
The traffic light is RED
*///:~
