package cn.feng.thinkInJava.b1_1_枚举类型.enumerated.a19_01_01;//: enumerated/Burrito.java
import static cn.feng.thinkInJava.b1_1_枚举类型.enumerated.a19_01_01.Spiciness.*;
public class Burrito {
  Spiciness degree;
  public Burrito(Spiciness degree) { this.degree = degree;}
  public String toString() { return "Burrito is "+ degree;}
  /**将静态导入用于enum :import static能够将enum实例的标识符带入当前的命名空间无需,所以无需再用enum修饰实例
   * 在定义enum的同一个文件中,无法使用,如果是在默认包中定义enum,这种技巧也无法使用
 *@author fengyu
 * @date  2015年8月9日
 * @param args
 */
public static void main(String[] args) {
    System.out.println(new Burrito(NOT));
    System.out.println(new Burrito(MEDIUM));
    System.out.println(new Burrito(HOT));
  }
} /* Output:
Burrito is NOT
Burrito is MEDIUM
Burrito is HOT
*///:~
