
//: enumerated/EnumMaps.java
// Basics of EnumMaps.
package cn.feng.thinkInJava.b1_1_枚举类型.enumerated.a19_09_00;

import static net.mindview.util.Print.print;
import static net.mindview.util.Print.printnb;

import java.util.EnumMap;
import java.util.Map;

import static cn.feng.thinkInJava.b1_1_枚举类型.enumerated.a19_09_00.AlarmPoints.*;

interface Command
{
	void action();
}

/**EnumMap是一种特殊的Map,他要求其中的键必须来自一个Enum,由于enum本身的限制,所以EnumMap在内部可由数组实现
 * ,因此EnumMap速度很快
 * @author fengyu
 * @date  2015年8月14日
 */
public class EnumMaps
{	//演示命令设计模式,命令模式首先需要一个只有单一方法的接口,然后从该接口实现具有各自不同的行为的多个子类
	public static void main(String[] args)
	{
		EnumMap<AlarmPoints,Command> em=new EnumMap<AlarmPoints,Command>(AlarmPoints.class);
		em.put(KITCHEN,new Command()
		{
			public void action()
			{
				print("Kitchen fire!");
			}
		});
		em.put(BATHROOM,new Command()
		{
			public void action()
			{
				print("Bathroom alert!");
			}
		});
		for(Map.Entry<AlarmPoints,Command> e:em.entrySet())
		{
			printnb(e.getKey()+": ");
			e.getValue().action();
		}
		try
		{ // If there's no value for a particular key:
			//如果没有为这个键调用put()方法来存入相应的值的话,其值就是null
			em.get(UTILITY).action();
		}
		catch(Exception e)
		{
			print(e);
		}
	}
} /*
 * Output: BATHROOM: Bathroom alert! KITCHEN: Kitchen fire!
 * java.lang.NullPointerException
 */// :~
