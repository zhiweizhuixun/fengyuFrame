package cn.feng.thinkInJava.b1_1_枚举类型.enumerated.a19_01_00;

//: enumerated/EnumClass.java
// Capabilities of the Enum class
import static net.mindview.util.Print.*;

enum Shrubbery
{
	GROUND,CRAWLING,HANGING
}

/**
 * 基本的enum特性 关键字enum可以将一组具名的值的有限集合创建为一种新的类型,而这些具名的只可以作为常规的程序组件使用
 * 
 * @author fengyu
 * @date 2015年8月9日
 */
public class EnumClass
{
	public static void main(String[] args)
	{
		for(Shrubbery s:Shrubbery.values())
		{
			print(s+" ordinal: "+s.ordinal());// ordinal返回一个int值这是每个enum实例在声明时候的值,从0开始
			printnb(s.compareTo(Shrubbery.CRAWLING)+" ");//Enum类实现了Comparable接口所以它具有compareTo()方法,
			printnb(s.equals(Shrubbery.CRAWLING)+" ");
			// 可以使用==来比较enum实例,编译器自动提供equals()和hashCode()方法,同时还实现了Serializable接口
			print(s==Shrubbery.CRAWLING);
			print(s.getDeclaringClass());//获取enum实例所属的enum类
			print(s.name());//方法返回enum实例的名字和tostring()方法效果相同
			print("----------------------");
		}
		// Produce an enum value from a string name:
		for(String s:"HANGING CRAWLING GROUND".split(" "))
		{
			//valueOf()是在Enum中定义的静态方法,它根据给定的名字返回相应的enum实例,如果不存在给定名字的实例将抛出异常
			Shrubbery shrub=Enum.valueOf(Shrubbery.class,s);
			print(shrub);
		}
	}
} /*
	 * Output: GROUND ordinal: 0 -1 false false class Shrubbery GROUND
	 * ---------------------- CRAWLING ordinal: 1 0 true true class Shrubbery
	 * CRAWLING ---------------------- HANGING ordinal: 2 1 false false class
	 * Shrubbery HANGING ---------------------- HANGING CRAWLING GROUND
	 */// :~
