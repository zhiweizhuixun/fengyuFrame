package cn.feng.thinkInJava.a9_2_泛型.generics.a15_04_03;

//: generics/ExplicitTypeSpecification.java
import cn.feng.thinkInJava.a9_1_类型信息.typeinfo.pets.*;
import java.util.*;
import net.mindview.util.*;

/**
 * 显式的类型说明:在泛型方法中显式的指明类型,不过这语法很少用
 * 
 * @author fengyu
 * @date 2015年8月9日
 */
public class ExplicitTypeSpecification
{
	static void f(Map<Person,List<Pet>> petPeople)
	{
	}
	
	public static void main(String[] args)
	{	
		//使用静态方法必须在点操作符之前加类名
		f(New.<Person,List<Pet>>map());
	}
} // /:~
