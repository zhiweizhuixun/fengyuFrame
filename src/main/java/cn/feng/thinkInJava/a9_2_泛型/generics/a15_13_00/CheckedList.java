package cn.feng.thinkInJava.a9_2_泛型.generics.a15_13_00;

//: generics/CheckedList.java
// Using Collection.checkedList().
import cn.feng.thinkInJava.a9_1_类型信息.typeinfo.pets.*;
import java.util.*;

/**
 * 动态类型安全
 * 
 * @author fengyu
 * @date 2015年8月9日
 */
public class CheckedList
{
	@SuppressWarnings("unchecked")
	static void oldStyleMethod(List probablyDogs)
	{
		probablyDogs.add(new Cat());
	}
	
	public static void main(String[] args)
	{
		List<Dog> dogs1=new ArrayList<Dog>();
		oldStyleMethod(dogs1); // Quietly accepts a Cat
		List<Dog> dogs2=Collections.checkedList(new ArrayList<Dog>(),Dog.class);
		try
		{
			oldStyleMethod(dogs2); // Throws an exception
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		// Derived types work fine:
		List<Pet> pets=Collections.checkedList(new ArrayList<Pet>(),Pet.class);
		pets.add(new Dog());
		// pets.add(new Cat());
	}
} /*
 * Output: java.lang.ClassCastException: Attempt to insert class
 * typeinfo.pets.Cat element into collection with element type class
 * typeinfo.pets.Dog
 */// :~
