package cn.feng.thinkInJava.a9_2_泛型.generics.a15_07_00;

//: generics/ErasedTypeEquivalence.java
import java.util.*;

/**
 * 泛型擦除的神秘之处
 * 
 * @author fengyu
 * @date 2015年8月9日
 */
public class ErasedTypeEquivalence
{
	public static void main(String[] args)
	{
		Class c1=new ArrayList<String>().getClass();
		Class c2=new ArrayList<Integer>().getClass();
		System.out.println(c1==c2);
	}
} /*
 * Output: true
 */// :~
