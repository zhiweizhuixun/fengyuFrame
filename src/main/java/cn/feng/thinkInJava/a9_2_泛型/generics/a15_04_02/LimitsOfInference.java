package cn.feng.thinkInJava.a9_2_泛型.generics.a15_04_02;

//: generics/LimitsOfInference.java
import cn.feng.thinkInJava.a9_1_类型信息.typeinfo.pets.*;
import java.util.*;

public class LimitsOfInference
{
	//将一个泛型方法调用的结果作为参数的时候,传递个另外一个方法,这时候编译器并不会执行类型推断
	//编译器认为:调用泛型方法后,其返回值被赋给一个Object类型的变量
	static void f(Map<Person,List<? extends Pet>> petPeople)
	{
	}
	
	public static void main(String[] args)
	{
		// f(New.map()); // Does not compile
	}
} /// :~
