package cn.feng.thinkInJava.a9_2_泛型.generics.a15_10_02;
//: generics/SuperTypeWildcards.java
import java.util.List;


/**逆变
 * @author fengyu
 * @date  2015年8月9日
 */
public class SuperTypeWildcards {
		//超类通配符
	//参数Apple是Apple的某种基类型的List,这样就知道向其中添加的Apple或Apple的子类型是安全的,,但是既然Apple是下界,那么就知道向
//	这样的List中添加是Fruit是不安全的,因为将使这个List敞开扣子,从而可以向其中添加一个非Apple类型的对象,而这是违反静态类型安全的
  static void writeTo(List<? super Apple> apples) {
    apples.add(new Apple());
    apples.add(new Jonathan());
    // apples.add(new Fruit()); // Error
  }
} ///:~
class Fruit
{
}

class Apple extends Fruit
{
}

class Jonathan extends Apple
{
}

class Orange extends Fruit
{
}