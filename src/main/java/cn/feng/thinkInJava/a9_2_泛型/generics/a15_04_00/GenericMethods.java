package cn.feng.thinkInJava.a9_2_泛型.generics.a15_04_00;

//: generics/GenericMethods.java
/**
 * 泛型方法:是否拥有泛型方法与其所在类是否是泛型没有关系,泛型方法能够独立类存在而产生变化
 * 基本指导原则:无论何时只要你能做到,你就应该尽量使用泛型方法,因为它可以使事情更清楚明白,
 * 另外,对于一个static的方法而言,需要使用泛型能力,就必须使其成为泛型方法
 * @author fengyu
 * @date 2015年8月9日
 */
public class GenericMethods
{
	//定义泛型方法,只需将泛型参数列表置于返回值之前
	public <T> void f(T x)
	{
		System.out.println(x.getClass().getName());
	}
	
	public static void main(String[] args)
	{
		GenericMethods gm=new GenericMethods();
		//使用泛型方法时候通常不必指明参数类型,因为编译器会为我们找出具体的类型,这称为类型参数推断(type argumens inference)
		gm.f("");
		gm.f(1);
		gm.f(1.0);
		gm.f(1.0F);
		gm.f('c');
		gm.f(gm);
	}
} /*
	 * Output: java.lang.String java.lang.Integer java.lang.Double
	 * java.lang.Float java.lang.Character GenericMethods
	 */// :~
