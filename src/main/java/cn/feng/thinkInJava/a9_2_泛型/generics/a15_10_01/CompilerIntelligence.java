	package cn.feng.thinkInJava.a9_2_泛型.generics.a15_10_01;

//: generics/CompilerIntelligence.java
import java.util.*;

public class CompilerIntelligence
{
	public static void main(String[] args)
	{
		List<? extends Fruit> flist=Arrays.asList(new Apple());
		Apple a=(Apple)flist.get(0); // No warning
		flist.contains(new Apple()); // Argument is 'Object'
		flist.indexOf(new Apple()); // Argument is 'Object'
	}
} /// :~

class Fruit
{
}

class Apple extends Fruit
{
}

class Jonathan extends Apple
{
}

class Orange extends Fruit
{
}
