package cn.feng.thinkInJava.a9_2_泛型.generics.a15_11_01;

//: generics/ListOfInt.java
// Autoboxing compensates for the inability to use
// primitives in generics.
import java.util.*;

/**泛型的问题:
 * 任何基本类型都不能作为类型参数
 * 
 * @author fengyu
 * @date 2015年8月9日
 */
public class ListOfInt
{
	public static void main(String[] args)
	{
		List<Integer> li=new ArrayList<Integer>();
		for(int i=0;i<5;i++)
			li.add(i);
		for(int i:li)
			System.out.print(i+" ");
	}
} /*
 * Output: 0 1 2 3 4
 */// :~
