package cn.feng.thinkInJava.a9_2_泛型.generics.a15_07_02;
//: generics/Manipulator3.java

class Manipulator3 {
  private HasF obj;
  public Manipulator3(HasF x) { obj = x; }
  public void manipulate() { obj.f(); }
} ///:~
