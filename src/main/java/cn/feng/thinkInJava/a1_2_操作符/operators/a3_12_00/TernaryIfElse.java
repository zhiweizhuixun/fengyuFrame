package cn.feng.thinkInJava.a1_2_操作符.operators.a3_12_00;
//: operators/TernaryIfElse.java
import static net.mindview.util.Print.*;

/**三元操作符if-else
 * @author fengyu
 * @date  2015年8月9日
 */
public class TernaryIfElse {
	
	//三元
  static int ternary(int i) {
    return i < 10 ? i * 100 : i * 10;
  }
  static int standardIfElse(int i) {
    if(i < 10)
      return i * 100;
    else
      return i * 10;
  }
  public static void main(String[] args) {
    print(ternary(9));
    print(ternary(10));
    print(standardIfElse(9));
    print(standardIfElse(10));
  }
} /* Output:
900
100
900
100
*///:~
