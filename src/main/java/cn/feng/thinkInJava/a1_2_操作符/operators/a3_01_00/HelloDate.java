package cn.feng.thinkInJava.a1_2_操作符.operators.a3_01_00;
//: operators/HelloDate.java
import java.util.*;
import static net.mindview.util.Print.*;

/**简单的打印语句
 * @author fengyu
 * @date  2015年8月9日
 */
public class HelloDate {
  public static void main(String[] args) {
    print("Hello, it's: ");
    print(new Date());
  }
} /* Output: (55% match)
Hello, it's:
Wed Oct 05 14:39:05 MDT 2005
*///:~
