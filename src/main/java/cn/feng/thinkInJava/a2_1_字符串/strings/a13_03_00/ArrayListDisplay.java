package cn.feng.thinkInJava.a2_1_字符串.strings.a13_03_00;

//: strings/ArrayListDisplay.java
import cn.feng.thinkInJava.a9_2_泛型.generics.coffee.*;
import cn.feng.thinkInJava.a9_2_泛型.generics.coffee.a15_03_00.CoffeeGenerator;

import java.util.*;

/**
 * 无意识的递归
 * 
 * @author fengyu
 * @date 2015年8月8日
 */
public class ArrayListDisplay
{
	public static void main(String[] args)
	{
		ArrayList<Coffee> coffees = new ArrayList<Coffee>();
		for (Coffee c : new CoffeeGenerator(10))
			coffees.add(c);
		System.out.println(coffees);
	}
} /*
 * Output: [Americano 0, Latte 1, Americano 2, Mocha 3, Mocha 4, Breve 5,
 * Americano 6, Latte 7, Cappuccino 8, Cappuccino 9]
 */// :~
