package cn.feng.thinkInJava.a2_1_字符串.strings.a13_05_03;

//: strings/DatabaseException.java
/**
 * String.format()
 * 
 * @author fengyu
 * @date 2015年8月8日
 */
public class DatabaseException extends Exception
{
	public DatabaseException( int transactionID, int queryID, String message )
	{
		super(String.format("(t%d, q%d) %s", transactionID, queryID, message));
	}
	
	public static void main(String[] args)
	{
		try
		{
			throw new DatabaseException(3, 7, "Write failed");
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
	}
} /*
 * Output: DatabaseException: (t3, q7) Write failed
 */// :~
