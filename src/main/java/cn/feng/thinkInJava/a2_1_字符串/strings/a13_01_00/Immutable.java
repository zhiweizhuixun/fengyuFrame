package cn.feng.thinkInJava.a2_1_字符串.strings.a13_01_00;

//: strings/Immutable.java
import static net.mindview.util.Print.*;

//不可变的String
public class Immutable
{
	public static String upcase(String s)
	{
		return s.toUpperCase();
	}
	
	public static void main(String[] args)
	{
		String q = "howdy";
		print(q); // howdy
		String qq = upcase(q);
		print(qq); // HOWDY
		print(q); // howdy
	}
} /*
 * Output: howdy HOWDY howdy
 */// :~
