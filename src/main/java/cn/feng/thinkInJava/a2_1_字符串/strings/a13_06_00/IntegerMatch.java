package cn.feng.thinkInJava.a2_1_字符串.strings.a13_06_00;
//: strings/IntegerMatch.java

/**正则表达式
 * @author fengyu
 * @date  2015年8月8日
 */
public class IntegerMatch
{
	public static void main(String[] args)
	{
		System.out.println("-1234".matches("-?\\d+"));
		System.out.println("5678".matches("-?\\d+"));
		System.out.println("+911".matches("-?\\d+"));
		System.out.println("+911".matches("(-|\\+)?\\d+"));
	}
} /* Output:
true
true
false
true
*///:~
