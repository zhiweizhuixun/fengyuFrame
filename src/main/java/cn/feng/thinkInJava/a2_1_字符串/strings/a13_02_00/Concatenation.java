package cn.feng.thinkInJava.a2_1_字符串.strings.a13_02_00;

//: strings/Concatenation.java
/**重载"+"与StringBuilder
 * @author fengyu
 * @date 2015年8月8日
 */
// 连结
public class Concatenation
{
	public static void main(String[] args)
	{
		String mango = "mango";
		String s = "abc" + mango + "def" + 47;
		System.out.println(s);
	}
} /*
 * Output: abcmangodef47
 */// :~
