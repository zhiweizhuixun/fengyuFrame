package cn.feng.thinkInJava.a7_3_持有对象.holding.b_11_01_00;



//: holding/ApplesAndOrangesWithGenerics.java
import java.util.ArrayList;

/**
 * 使用泛型:通过使用泛型,就可以在编译期间防止将错误类型对象放置到容器中
 * 
 * @author fengyu
 * @param <ApplesAndOrangesWithGenerics>
 * @date 2015年8月8日
 */

public class ApplesAndOrangesWithGenerics {
	  public static void main(String[] args) {
	    ArrayList<Apple> apples = new ArrayList<Apple>();
	    for(int i = 0; i < 3; i++)
	      apples.add(new Apple());
	    // Compile-time error:
	    // apples.add(new Orange());
	    for(int i = 0; i < apples.size(); i++)
	      System.out.println(apples.get(i).id());
	    // Using foreach:
	    for(Apple c : apples)
	      System.out.println(c.id());
	  }
	} /* Output:
	0
	1
	2
	0
	1
	2
	*///:~

