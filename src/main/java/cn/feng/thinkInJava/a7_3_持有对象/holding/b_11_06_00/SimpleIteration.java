package cn.feng.thinkInJava.a7_3_持有对象.holding.b_11_06_00;

//: holding/SimpleIteration.java
import java.util.Iterator;
import java.util.List;

import cn.feng.thinkInJava.a9_1_类型信息.typeinfo.pets.Pet;
import cn.feng.thinkInJava.a9_1_类型信息.typeinfo.pets.Pets;

/**
 * 迭代器展示
 */
public class SimpleIteration {
	public static void main(String[] args) {
		List<Pet> pets = Pets.arrayList(12);

		// java 的Iterator只能单向移动
		Iterator<Pet> it = pets.iterator();// iterator();返回一个Iterator
		while (it.hasNext()) {// 检查序列中是否还有元素
			Pet p = it.next();// 获取序列的下个元素
			System.out.print(p.id() + ":" + p + " ");
		}
		System.out.println();
		// A simpler approach, when
		// possible:如果只是向前遍历List,并不打算修改List对象本身,可以使用foreach语法,会显得更加简洁
		for (Pet p : pets)
			System.out.print(p.id() + ":" + p + " ");
		System.out.println();
		// An Iterator can also remove elements:
		it = pets.iterator();
		for (int i = 0; i < 6; i++) {
			it.next();
			it.remove();// 将迭代器新近返回的元素删除
		}
		System.out.println(pets);
	}
} /*
 * Output: 0:Rat 1:Manx 2:Cymric 3:Mutt 4:Pug 5:Cymric 6:Pug 7:Manx 8:Cymric
 * 9:Rat 10:EgyptianMau 11:Hamster 0:Rat 1:Manx 2:Cymric 3:Mutt 4:Pug 5:Cymric
 * 6:Pug 7:Manx 8:Cymric 9:Rat 10:EgyptianMau 11:Hamster [Pug, Manx, Cymric,
 * Rat, EgyptianMau, Hamster]
 */// :~
