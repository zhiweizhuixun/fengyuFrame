package cn.feng.thinkInJava.b3_1_多线程并发.concurrency.a21_06_05;
//: concurrency/Chopstick.java
// Chopsticks for dining philosophers.

/**死锁
 * @author fengyu
 * @date  2015年8月9日
 */
public class Chopstick {
  private boolean taken = false;
  public synchronized
  void take() throws InterruptedException {
    while(taken)
      wait();
    taken = true;
  }
  public synchronized void drop() {
    taken = false;
    notifyAll();
  }
} ///:~
