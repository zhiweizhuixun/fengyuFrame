package cn.feng.thinkInJava.b3_1_多线程并发.concurrency.a21_02_09;
//: concurrency/SimpleThread.java
// Inheriting directly from the Thread class.

/**编码的变体:直接从Thread继承这种可替换,可以通过调用适当的Thread构造器Thread对象赋予具体的名称,这个
 * 名称可以通过使用getName()从toString()中获得
 * 
 * @author fengyu
 * @date  2015年8月9日
 */
public class SimpleThread extends Thread {
  private int countDown = 5;
  private static int threadCount = 0;
  public SimpleThread() {
    // Store the thread name:
    super(Integer.toString(++threadCount));
    start();
  }
  public String toString() {
    return "#" + getName() + "(" + countDown + "), ";
  }
  public void run() {
    while(true) {
      System.out.print(this);
      if(--countDown == 0)
        return;
    }
  }
  public static void main(String[] args) {
    for(int i = 0; i < 5; i++)
      new SimpleThread();
  }
} /* Output:
#1(5), #1(4), #1(3), #1(2), #1(1), #2(5), #2(4), #2(3), #2(2), #2(1), #3(5), #3(4), #3(3), #3(2), #3(1), #4(5), #4(4), #4(3), #4(2), #4(1), #5(5), #5(4), #5(3), #5(2), #5(1),
*///:~
