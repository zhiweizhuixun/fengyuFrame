package cn.feng.thinkInJava.b3_1_多线程并发.concurrency.a21_03_02;

import cn.feng.thinkInJava.b3_1_多线程并发.concurrency.a21_03_00.EvenChecker;
import cn.feng.thinkInJava.b3_1_多线程并发.concurrency.a21_03_00.IntGenerator;
//: concurrency/SynchronizedEvenGenerator.java
// Simplifying mutexes with the synchronized keyword.
// {RunByHand}

public class
SynchronizedEvenGenerator extends IntGenerator {
  private int currentEvenValue = 0;
  public synchronized int next() {
    ++currentEvenValue;
    Thread.yield(); // Cause failure faster
    ++currentEvenValue;
    return currentEvenValue;
  }
  public static void main(String[] args) {
    EvenChecker.test(new SynchronizedEvenGenerator());
  }
} ///:~
