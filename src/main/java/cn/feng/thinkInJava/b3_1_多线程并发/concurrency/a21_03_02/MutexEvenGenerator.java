package cn.feng.thinkInJava.b3_1_多线程并发.concurrency.a21_03_02;
//: concurrency/MutexEvenGenerator.java
// Preventing thread collisions with mutexes.
// {RunByHand}
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import cn.feng.thinkInJava.b3_1_多线程并发.concurrency.a21_03_00.EvenChecker;
import cn.feng.thinkInJava.b3_1_多线程并发.concurrency.a21_03_00.IntGenerator;

/**使用显式的LOck对象
 * @author fengyu
 * @date  2015年8月9日
 */
public class MutexEvenGenerator extends IntGenerator {
  private int currentEvenValue = 0;
  private Lock lock = new ReentrantLock();
  public int next() {
    lock.lock();
    try {
      ++currentEvenValue;
      Thread.yield(); // Cause failure faster
      ++currentEvenValue;
      return currentEvenValue;
    } finally {
      lock.unlock();
    }
  }
  public static void main(String[] args) {
    EvenChecker.test(new MutexEvenGenerator());
  }
} ///:~
