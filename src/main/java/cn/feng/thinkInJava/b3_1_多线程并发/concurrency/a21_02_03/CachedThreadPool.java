package cn.feng.thinkInJava.b3_1_多线程并发.concurrency.a21_02_03;
//: concurrency/CachedThreadPool.java
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import cn.feng.thinkInJava.b3_1_多线程并发.concurrency.a21_02_01.LiftOff;

/**使用(执行器)Executor管理你的Thread对象,从而优化了并发编程
 * 
 * Executors在客户端和任务执行之间提供了一个间接层;与客户端直接执行任务不同,这个中介对象将执行任务
 * Executor允许你管理异步任务的执行,而无需显式地管理线程生命周期.Executor在javaSE 5/6中是启动任务的优先方法
 * @author fengyu
 * @date  2015年8月9日
 */
public class CachedThreadPool {
  public static void main(String[] args) {
	  //非常常见的情况是,单个的Executor被用来创建和管理系统中所有的任务
    ExecutorService exec = Executors.newCachedThreadPool();
    for(int i = 0; i < 5; i++)
      exec.execute(new LiftOff());
    exec.shutdown();
  }
} /* Output: (Sample)
#0(9), #0(8), #1(9), #2(9), #3(9), #4(9), #0(7), #1(8), #2(8), #3(8), #4(8), #0(6), #1(7), #2(7), #3(7), #4(7), #0(5), #1(6), #2(6), #3(6), #4(6), #0(4), #1(5), #2(5), #3(5), #4(5), #0(3), #1(4), #2(4), #3(4), #4(4), #0(2), #1(3), #2(3), #3(3), #4(3), #0(1), #1(2), #2(2), #3(2), #4(2), #0(Liftoff!), #1(1), #2(1), #3(1), #4(1), #1(Liftoff!), #2(Liftoff!), #3(Liftoff!), #4(Liftoff!),
*///:~
