package cn.feng.thinkInJava.a7_3_异常.exceptions.a12_06_01;
//: exceptions/WhoCalled.java
// Programmatic access to stack trace information.

/**栈轨迹
 * @author fengyu
 * @date  2015年8月8日
 */
public class WhoCalled
{
	static void f()
	{
		// Generate an exception to fill in the stack trace
		try
		{
			throw new Exception();
		}
		catch (Exception e)
		{
			for (StackTraceElement ste : e.getStackTrace())
				System.out.println(ste.getMethodName());
		}
	}
	
	static void g()
	{
		f();
	}
	
	static void h()
	{
		g();
	}
	
	public static void main(String[] args)
	{
		f();
		System.out.println("--------------------------------");
		g();
		System.out.println("--------------------------------");
		h();
	}
} /* Output:
f
main
--------------------------------
f
g
main
--------------------------------
f
g
h
main
*///:~
