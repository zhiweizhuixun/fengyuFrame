package cn.feng.thinkInJava.a7_3_异常.exceptions.a12_07_00;
//: exceptions/NeverCaught.java
// Ignoring RuntimeExceptions.
// {ThrowsException}

/**java标准异常
 * @author fengyu
 * @date  2015年8月8日
 */
public class NeverCaught {
  static void f() {
    throw new RuntimeException("From f()");
  }
  static void g() {
    f();
  }
  public static void main(String[] args) {
    g();
  }
} ///:~
