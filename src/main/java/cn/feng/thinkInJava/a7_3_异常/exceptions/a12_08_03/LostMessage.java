package cn.feng.thinkInJava.a7_3_异常.exceptions.a12_08_03;

//: exceptions/LostMessage.java
// How an exception can be lost.
/**
 * 缺憾:异常缺失
 * 
 * @author fengyu
 * 
 * 
 * 
 * @date 2015年8月8日
 */
class VeryImportantException extends Exception
{
	public String toString()
	{
		return "A very important exception!";
	}
}

class HoHumException extends Exception
{
	public String toString()
	{
		return "A trivial exception";
	}
}

public class LostMessage
{
	void f() throws VeryImportantException
	{
		throw new VeryImportantException();
	}
	
	void dispose() throws HoHumException
	{
		throw new HoHumException();
	}
	
	public static void main(String[] args)
	{
		try
		{
			LostMessage lm = new LostMessage();
			try
			{
				lm.f();
			}
			finally
			{
				lm.dispose();
			}
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
	}
} /*
 * Output: A trivial exception
 */// :~
